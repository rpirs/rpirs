"""
A module for robust phase identification by Raman spectroscopy
(October 26th, 2017).

.. codeauthor:: Gustavo R. Ramos <grramos@ucs.br>

Warning:
    Statement QtCore.QCoreApplication.processEvents() is frequently considered
    as a last resort for refreshing the GUI, but is used in current version of
    code.

Note:
    The need for wrapping all assert statements with the try-except clause is
    because when using a "normal" assert statement the error message is not
    printed to the GUI. This was the best workaround found.

Todo:
    Number of candidates is being defined in the code. It should be defined in
    input file.

Todo:
    The default value of argument type (="dot") is being used for the
    matchFactor function of ALS package. Let the user choose between "dot" and
    "euclid" in input file and, then, check if it is proper to normalize plot
    of component and candidate spectra.

Todo:
    Check if time.time() is the better option for measuring a process time.

Todo:
    The muppy module is able to identify memory leaks. Test it.

Todo:
    Is it Ok to perform the numpy version check in the module preamble?
"""

import re
import time
import numpy as np
from PyQt5 import QtCore

# According to rpy2 manual, "rpy2 is providing 2 levels of interface with R:
# - low-level (rpy2.interface) - high-level (rpy2.objects). The high-level
# interface is trying to make the use of R as natural as possible for a Python
# user [...]. Importing the top-level subpackage is also initializing and
# starting R embedded in the current Python process:"
import rpy2.robjects as robjects

# According to rpy2 manual, "importing R packages is often the first step when
# running R code, and rpy2 is providing a function
# rpy2.robjects.packages.importr() that makes that step very similar to
# importing Python packages".
from rpy2.robjects.packages import importr

# According to rpy2 manual, "the activation (and deactivation) of the
# automatic conversion of numpy objects into rpy2 objects can be made with:"
from rpy2.robjects import numpy2ri
numpy2ri.activate()
numpy2ri.deactivate()

NUMBER_CANDIDATES = 10

# Check if numpy version is newer than 1.7, for being sure that the dot method
# is safely applied to sparse matrices, as done in baseline_als method. (See
# https://docs.scipy.org/doc/scipy-0.19.0/reference/sparse.html.)
np_version_required = '1.7'.split('.')
np_version = np.__version__.split('.')[:2]
for field_i, field in enumerate(np_version):
    if np.int(field) > np.int(np_version_required[field_i]):
        break
    elif np.int(field) < np.int(np_version_required[field_i]):
        raise RuntimeError('Numpy version is not newer than 1.7.')
else:
    raise RuntimeError('Numpy version is not newer than 1.7.')

def str_to_bool(value):
    """Return bool value from str.

    Args:
        value (str): str that should be equal to 'True' or 'False'.

    Returns:
        bool: bool corresponding to value.

    Example:
        >>> str_to_bool('True')
        True
        >>> str_to_bool('False')
        False
        >>> str_to_bool('Wrong Input')
        Traceback (most recent call last):
            ...
        AssertionError
        >>> str_to_bool(2)
        Traceback (most recent call last):
            ...
        AssertionError
    """

    try:
        assert isinstance(value, str)
    except AssertionError:
        print('AssertionError: Argument value ({}) sould be of str '
              'type.'.format(value))
        raise

    try:
        assert value in ['True', 'False']
    except AssertionError:
        print("AssertionError: Argument value ({}) must be equal to either "
              "'True' or 'False'.".format(value))
        raise

    if value == 'True':
        return True
    elif value == 'False':
        return False

def get_chemical_elements_in_line(line):
    """Get the chemical elements present in line.

    Args:
        line (str): Line of the database file which contains the ideal
            chemical composition.

    Returns:
        list: List with elements present after 'IDEAL CHEMISTRY' substring.
    """

    try:
        assert isinstance(line, str)
    except AssertionError:
        print('AssertionError: Argument line ({}) should be of str '
              'type.'.format(line))
        raise

    try:
        assert line
    except AssertionError:
        print('AssertionError: Argument line must not have zero length.')
        raise

    field = re.search('IDEAL CHEMISTRY', line)
    assert field is not None, \
        "Did not find substring 'IDEAL CHEMISTRY' in argument line " \
        "({}).".format(line)
    ideal_chemistry = field.string[field.end():]

    return get_elements_in_string(ideal_chemistry)

def get_elements_in_string(ideal_chemistry):
    """Get the chemical elements present in ideal_chemistry.

    Args:
        ideal_chemistry (str): Partial line of database file, containing the
            string after 'IDEAL CHEMISTRY'.

    Returns:
        list: List with elements present in ideal_chemistry.
    """

    elements_NIST = \
        ['H', 'He',
         'Li', 'Be', 'B', 'C', 'N', 'O', 'F', 'Ne',
         'Na', 'Mg', 'Al', 'Si', 'P', 'S', 'Cl', 'Ar',
         'K', 'Ca', 'Sc', 'Ti', 'V', 'Cr', 'Mn', 'Fe', 'Co', 'Ni', 'Cu', 'Zn',
         'Ga', 'Ge', 'As', 'Se', 'Br', 'Kr',
         'Rb', 'Sr', 'Y', 'Zr', 'Nb', 'Mo', 'Tc', 'Ru', 'Rh', 'Pd', 'Ag',
         'Cd', 'In', 'Sn', 'Sb', 'Te', 'I', 'Xe',
         'Cs', 'Ba', 'La', 'Ce', 'Pr', 'Nd', 'Pm', 'Sm', 'Eu', 'Gd', 'Tb',
         'Dy', 'Ho', 'Er', 'Tm', 'Yb', 'Lu', 'Hf', 'Ta', 'W', 'Re', 'Os',
         'Ir', 'Pt', 'Au', 'Hg', 'Tl', 'Pb', 'Bi', 'Po', 'At', 'Rn',
         'Fr', 'Ra', 'Ac', 'Th', 'Pa', 'U', 'Np', 'Pu', 'Am', 'Cm', 'Bk',
         'Cf', 'Es', 'Fm', 'Md', 'No', 'Lr', 'Rf', 'Db', 'Sg', 'Bh', 'Hs',
         'Mt', 'Ds', 'Rg', 'Cn', 'Nh', 'Fl', 'Mc', 'Lv', 'Ts', 'Og']

    try:
        assert isinstance(ideal_chemistry, str)
    except AssertionError:
        print('AssertionError: Argument ideal_chemistry ({}) should be of '
              'str type.'.format(ideal_chemistry))
        raise

    try:
        assert ideal_chemistry
    except AssertionError:
        print('AssertionError: Argument ideal_chemistry must not have zero '
              'length.')
        raise

    # Some entries in RRUFF database contain the substring 'REE', which means
    # Rare Earth Element. When this is the case, the approach followed here is
    # simply to remove the substring from the list of elements present in the
    # ideal chemical composition.
    ideal_chemistry = ideal_chemistry.replace('REE', '')

    elements = re.findall(r'[A-Z][a-z]*', ideal_chemistry)

    try:
        assert elements
    except AssertionError:
        print('AssertionError: Did not find any element in chemical '
              'composition of database entry.')
        raise

    for element in elements:
        try:
            assert element in elements_NIST
        except AssertionError:
            print('AssertionError: Element {} is not valid.'.format(element))
            raise

    return sorted(list(set(elements)))

def baseline_als(y, lam, p, tol, max_iter):
    """Perform baseline fitting.

    Perform baseline fitting based on 'Baseline Correction with Asymmetric
    Least Squares Smoothing', by Paul H. C. Eilers and Hans F. M. Boelens.
    According to this reference, if the data are denoted by :math:`y_i` and
    the baseline by :math:`z_i`, the function to be minimized for a proper
    baseline fitting is

    .. math::
        S = \\sum_i w_i \\left( y_i - z_i \\right)^2 + \lambda \\sum_i
            \\left( \\Delta^2 z_i \\right)^2,

    where

    .. math::
        \\Delta^2 z_i = z_i - 2 z_{i-1} + z_{i-2}.

    The weights are defined by :math:`w_i = p` if :math:`y_i > z_i` and
    :math:`w_i = 1 - p` otherwise. The minimization of :math:`S` leads to

    .. math::
        - 2 \\sum_i w_i \\frac{\\partial z_i}{\\partial z_j}
            \\left( y_i - z_i \\right) + 2 \lambda \\sum_i
            \\frac{\\partial \\left( \\Delta^2 z_i \\right)}
            {\\partial z_j} \\Delta^2 z_i = 0.

    But

    .. math::
        \\frac{\\partial z_i}{\\partial z_j} = \\delta_{ij}

    and, then, we can write

    .. math::
        w_i \\frac{\\partial z_i}{\\partial z_j} = w_i \\delta_{ij}
            \colon = W_{ji}.

    On the other hand, it is also possible to write

    .. math::
        \\Delta^2 z_i = \\sum_k D_{ik} z_k,

    where :math:`\\mathbf{D}` is a difference matrix. From this it follows
    that

    .. math::
        \\frac{\\partial \\left( \\Delta^2 z_i \\right)}{\\partial z_j} =
            \\frac{\\partial \\left( \\sum_k D_{ik} z_k \\right)}
            {\\partial z_j} = \\sum_k D_{ik} \\frac{\\partial z_k}
            {\\partial z_j} = \\sum_k D_{ik} \\delta_{jk} = D_{ij}.

    With the relations just shown, it is possible to write

    .. math::
        - 2 \\sum_i W_{ji} \\left( y_i - z_i \\right) + 2 \lambda \\sum_i
            D_{ij} \\sum_k D_{ik} z_k = 0.

    Cancelling the factor 2, the matrix form of these equations (spanning all
    values of :math:`j`) is

    .. math::
        - \\mathbf{W} \\left( \\mathbf{y} - \\mathbf{z} \\right) + \lambda
            \\mathbf{D}^\\text{T} \\mathbf{D} \\mathbf{z} & =
            \\mathbf{0} \\\\
            \\left( \\mathbf{W} + \lambda \\mathbf{D}^\\text{T} \\mathbf{D}
            \\right) \\mathbf{z} & = \\mathbf{W} \\mathbf{y}.

    Args:
        y (numpy.ndarray): Vector containing the intensities of the Raman
            spectrum.
        lam (float): lambda parameter of algorithm.
        p (float): p parameter of algorithm.
        tol (float): Relative tolerance in weight for convergence of the
            algorithm.
        max_iter (int): Maximum number of iterations in algorithm.

    Returns:
        numpy.ndarray: Vector containing fitted baseline intensities.

    Warning:
        In current code, :math:`w_i` is defined by :math:`w_i = p` if
        :math:`y_i \ge z_i` and :math:`w_i = 1 - p` if :math:`y_i < z_i`.

    Example:
        The solution below is compared to a solution implemented in Octave, in
        accordance with the code supplied in the reference, with the exception
        that the iterative process is stopped if the relative tolerance in
        weight is achieved.

        >>> y = np.array([1, 0.5, 2.0, 3.0, 2.5], dtype=np.float)
        >>> z = baseline_als(y, 1e7, 0.05, 1e-6, 10)
            Baseline fitting converged with 2 iterations.
        >>> for el in z:
        ...     print('{:.6f}'.format(el))
        -0.021716
        0.617188
        1.256091
        1.894995
        2.533898

    Example:
        The solution below is compared to a solution implemented in Octave, in
        accordance with the code supplied in the reference, with the exception
        that the iterative process is stopped if the relative tolerance in
        weight is achieved.

        >>> y = np.array([1, 2, 3, 4, 5], dtype=np.float)
        >>> z = baseline_als(y, 1e7, 0.05, 1e-6, 10)
            Baseline fitting reached maximum number of iterations (10).
        >>> for el in z:
        ...     print('{:.6f}'.format(el))
        1.000000
        2.000000
        3.000000
        4.000000
        5.000000

    Example:
        The solution below is compared to a solution implemented in Octave, in
        accordance with the code supplied in the reference, with the exception
        that the iterative process is stopped if the relative tolerance in
        weight is achieved.

        >>> y = np.array([1, 1, 1, 1, 1], dtype=np.float)
        >>> z = baseline_als(y, 1e7, 0.05, 1e-6, 10)
            Baseline fitting reached maximum number of iterations (10).
        >>> for el in z:
        ...     print('{:.6f}'.format(el))
        1.000000
        1.000000
        1.000000
        1.000000
        1.000000
    """

    from scipy.sparse import csc_matrix, spdiags
    from scipy.sparse.linalg import spsolve

    try:
        assert isinstance(y, np.ndarray)
    except AssertionError:
        print('AssertionError: Argument y should be of type numpy.ndarray.')
        raise

    try:
        assert isinstance(lam, float)
    except AssertionError:
        print('AssertionError: Argument lam should be of type float.')
        raise

    try:
        assert isinstance(p, float)
    except AssertionError:
        print('AssertionError: Argument p should be of type float.')
        raise

    try:
        assert isinstance(tol, float)
    except AssertionError:
        print('AssertionError: Argument tol should be of type float.')
        raise

    try:
        assert isinstance(max_iter, int)
    except AssertionError:
        print('AssertionError: Argument max_iter should be of type int.')
        raise

    L = len(y)
    D = csc_matrix(np.diff(np.eye(L), n=2, axis=0), dtype=np.float)
    # Use a vector of ones as the initial guess for weights:
    w = np.ones(L)

    for i in range(max_iter):
        QtCore.QCoreApplication.processEvents()
        # A z = b
        w0 = w
        W = spdiags(w, 0, L, L)
        A = W + lam * D.transpose().dot(D)
        b = W.dot(y)
        z = spsolve(A, b)
        w = p * (y >= z) + (1 - p) * (y < z)
        if min(np.abs(w0)) > 0:
            if np.max(np.abs((w - w0) / w0)) <= tol:
                print('    Baseline fitting converged with {:d}'
                      ' iterations.'.format(i+1))
                return z

    print('    Baseline fitting reached maximum number of iterations'
          ' ({:d}).'.format(max_iter))

    return z

def plot_from_args(plot_arguments):
    """Plot graph based on plot_arguments.

    Arguments:
        plot_arguments (dict): Dictionary containing keys 'x', 'y', 'color',
            and 'label', which are used for plotting the figure.
    """

    try:
        assert isinstance(plot_arguments, dict)
    except AssertionError:
        print('AssertionError: Argument plot_arguments should be of dict '
              'type.')
        raise

    import matplotlib.pyplot as plt
    plt.plot(plot_arguments['x'],
             plot_arguments['y'],
             color=plot_arguments['color'],
             label=plot_arguments['label'])
    plt.legend()

class InputGeneral(object):
    """Handle input_file.

    Args:
        input_file (str): File (with full path) containing info about the
            other attributes in this class.
        directory (str): Path to directory where spectra are stored.
        file_names (str): File names (e.g.: '*.txt'). The program will try to
            read for analysis all files with this names stored in
            self.directory. The files must consist of two columns of numbers
            separated by self.column_delimiter, the first being the Raman
            shift and the second, the intensity.
        column_delimiter (str): Column delimiter (e.g.: ',') in data files.
        Rs_min (float): Minimum value for Raman shift in analysis.
        Rs_max (float): Maximum value for Raman shift in analysis.
        Rs_step (float): Step for Raman shift in analysis.

    Warning:
        According to the description of argument file_names, the program will
        try to read for analysis all files with names self.file_names located
        in self.directory. So, be sure that all files that meet these
        requirements are indeed intended to be analyzed. Otherwise, spurious
        data may be analyzed.

    Example:
        >>> InputGeneral('temp.txt')
        Traceback (most recent call last):
            ...
        AssertionError

    Example:
        >>> input = InputGeneral('test/aux_files_for_tests/Raman_experiment.rpirs')
        *** Reading input file...
            directory = /home/Desktop
            file_names = *.txt
            column_delimiter = ,
            Rs_min = 200.0
            Rs_max = 1200.0
            Rs_step = 0.5
        *** Finished reading input file.
        <BLANKLINE>

    Example:
        >>> input = InputGeneral('test/aux_files_for_tests/Raman_experiment_wrong_format.rpirs')
        Traceback (most recent call last):
            ...
        AssertionError
    """

    def __init__(self, input_file):

        import os

        try:
            assert isinstance(input_file, str)
        except AssertionError:
            print('AssertionError: Argument input_file ({}) sould be of str ' \
                  'type.'.format(input_file))
            raise

        try:
            assert os.path.isfile(input_file)
        except AssertionError:
            print('AssertionError: The argument input_file ({}) is not an '
                  'existing file.'.format(input_file))
            raise

        self.input_file = input_file
        self.directory = None
        self.file_names = None
        self.column_delimiter = None
        self.Rs_min = None
        self.Rs_max = None
        self.Rs_step = None

        self.read_input_file()

    def read_input_file(self):
        """Read input file for experimental or database Raman spectra."""

        print('*** Reading input file...')

        tags_in_file = {'# directory': False,
                        '# file_names': False,
                        '# column_delimiter': False,
                        '# Rs_min': False,
                        '# Rs_max': False,
                        '# Rs_step': False}

        with open(self.input_file, mode='r', encoding='utf8') as f:

            lines = f.readlines()

            for line_id, line in enumerate(lines):

                QtCore.QCoreApplication.processEvents()
                line_stripped = line.strip(' \t\n')

                if line_id < len(lines)-1:
                    next_line_stripped = lines[line_id+1].strip(' \t\n')

                if line_stripped == '# directory':
                    directory = next_line_stripped
                    print('    directory = {}'.format(directory))
                    tags_in_file['# directory'] = True

                elif line_stripped == '# file_names':
                    file_names = next_line_stripped
                    print('    file_names = {}'.format(file_names))
                    tags_in_file['# file_names'] = True

                elif line_stripped == '# column_delimiter':
                    # As the space is a possible column delimiter, the
                    # stripping process is different from the default.
                    column_delimiter = lines[line_id+1].strip('\n')
                    print('    column_delimiter = {}'.format(
                        column_delimiter))
                    tags_in_file['# column_delimiter'] = True

                elif line_stripped == '# Rs_min':
                    Rs_min = np.float(next_line_stripped)
                    print('    Rs_min = {}'.format(Rs_min))
                    tags_in_file['# Rs_min'] = True

                elif line_stripped == '# Rs_max':
                    Rs_max = np.float(next_line_stripped)
                    print('    Rs_max = {}'.format(Rs_max))
                    tags_in_file['# Rs_max'] = True

                elif line_stripped == '# Rs_step':
                    Rs_step = np.float(next_line_stripped)
                    print('    Rs_step = {}'.format(Rs_step))
                    tags_in_file['# Rs_step'] = True

        for key in tags_in_file:
            try:
                assert tags_in_file[key]
            except AssertionError:
                print('AssertionError: Tag "{}" is not present in input '
                      'file.'.format(key))
                raise

        self.directory = directory
        self.file_names = file_names
        self.column_delimiter = column_delimiter.replace('\\t', '\t')
        self.Rs_min = Rs_min
        self.Rs_max = Rs_max
        self.Rs_step = Rs_step

        print('*** Finished reading input file.\n')


class SpectraGeneral(object):
    """Handle experimental or database Raman spectra.

    Args:
        input_file (str): File (with full path) containing info about the
            attributes of InputGeneral class.
        input (InputGeneral instance): InputGeneral instance.
        files (str list): List of file names.
        _data (list): Spectra data.
        Rs (numpy.ndarray): Raman shift values.
        intensity_matrix (numpy.ndarray): Matrix of size n_spectra x
            n_data_points, in which each line holds the intensity values of a
            Raman spectrum after interpolation. The reference for
            interpolation is defined by self.Rs.
        n_data_points (int): Number of data points in each spectrum. This
            number depends on self.input.Rs_min, self.input.Rs_max, and
            self.input.Rs_step.
        n_spectra (int): Number of spectra.

    Note:
        self._data is created during reading process and later used for
        creating self.intensity_matrix.

    Example:
        (The plot generated by the following example demonstrates the correct
        interpolation of data.)

        >>> database = SpectraGeneral('test/aux_files_for_tests/'
        ...     'spectra/Raman_database.rpirs')
        >>> database.load() # doctest: +ELLIPSIS
        *** Reading input file...
            directory = test/aux_files_for_tests/spectra
            file_names = *.txt
            column_delimiter = ,
            Rs_min = 200.0
            Rs_max = 1200.0
            Rs_step = 0.5
        *** Finished reading input file.
        <BLANKLINE>
        *** Reading data...
            2.857 %
            100.000%
            ... seconds for reading data.
        *** Finished reading data.
        <BLANKLINE>
        *** Creating Raman shift values...
            ... seconds for creating Raman shift values.
        *** Finished creating Raman shift values.
        <BLANKLINE>
        *** Creating intensity matrix...
            ... seconds for creating intensity matrix.
        *** Finished creating intensity matrix.
        <BLANKLINE>
        >>> print(len(database.files))
        35
        >>> for file in database.files:
        ...     print(file)
        test/aux_files_for_tests/spectra/Abelsonite__R070007__Raman__532__0__unoriented__Raman_Data_Processed__27040.txt
        test/aux_files_for_tests/spectra/Abelsonite__R070007__Raman__785__0__unoriented__Raman_Data_Processed__25958.txt
        test/aux_files_for_tests/spectra/Abhurite__R060227__Raman__532__0__unoriented__Raman_Data_Processed__33408.txt
        test/aux_files_for_tests/spectra/Abhurite__R060227__Raman__780__0__unoriented__Raman_Data_Processed__33410.txt
        test/aux_files_for_tests/spectra/Abramovite__R070037__Raman__532__0__unoriented__Raman_Data_Processed__25038.txt
        test/aux_files_for_tests/spectra/Acanthite__R070578__Raman__532__0__unoriented__Raman_Data_Processed__23032.txt
        test/aux_files_for_tests/spectra/Acanthite__R070578__Raman__780__0__unoriented__Raman_Data_Processed__28322.txt
        test/aux_files_for_tests/spectra/Brianyoungite__R060431__Raman__532__0__unoriented__Raman_Data_Processed__33899.txt
        test/aux_files_for_tests/spectra/Brianyoungite__R060431__Raman__780__0__unoriented__Raman_Data_Processed__33900.txt
        test/aux_files_for_tests/spectra/CuZnClOH3__R140401__Raman__532__0__unoriented__Raman_Data_Processed__38047.txt
        test/aux_files_for_tests/spectra/CuZnClOH3__R140401__Raman__780__0__unoriented__Raman_Data_Processed__38049.txt
        test/aux_files_for_tests/spectra/Hainite-(Y)__R141183__Raman__532__0__unoriented__Raman_Data_Processed__38381.txt
        test/aux_files_for_tests/spectra/Hainite-(Y)__R141183__Raman__780__0__unoriented__Raman_Data_Processed__38389.txt
        test/aux_files_for_tests/spectra/Hydrokenoralstonite__R060012__Raman__532__0__unoriented__Raman_Data_Processed__26480.txt
        test/aux_files_for_tests/spectra/Hydrokenoralstonite__R060012__Raman__780__0__unoriented__Raman_Data_Processed__26482.txt
        test/aux_files_for_tests/spectra/Lorenzenite__R130051__Raman__532__0__unoriented__Raman_Data_Processed__37658.txt
        test/aux_files_for_tests/spectra/Lorenzenite__R130051__Raman__780__0__unoriented__Raman_Data_Processed__37660.txt
        test/aux_files_for_tests/spectra/Lorenzenite__R130051__Raman__780__0__unoriented__Raman_Data_RAW__37661.txt
        test/aux_files_for_tests/spectra/Manandonite__R060968__Raman__532__0__unoriented__Raman_Data_Processed__13822.txt
        test/aux_files_for_tests/spectra/Manandonite__R060968__Raman__785__0__unoriented__Raman_Data_Processed__13818.txt
        test/aux_files_for_tests/spectra/Mosandritece__R100097__Raman__532__0__unoriented__Raman_Data_Processed__34351.txt
        test/aux_files_for_tests/spectra/Mosandritece__R100097__Raman__780__0__unoriented__Raman_Data_RAW__34352.txt
        test/aux_files_for_tests/spectra/Mozartite__R070265__Raman__532__0__unoriented__Raman_Data_Processed__20256.txt
        test/aux_files_for_tests/spectra/Mozartite__R070265__Raman__785__0__unoriented__Raman_Data_Processed__25726.txt
        test/aux_files_for_tests/spectra/Namuwite__R130173__Raman__532__0__unoriented__Raman_Data_Processed__37896.txt
        test/aux_files_for_tests/spectra/Namuwite__R130173__Raman__780__0__unoriented__Raman_Data_Processed__37898.txt
        test/aux_files_for_tests/spectra/Rinkitece__R050229__Raman__514__0__unoriented__Raman_Data_Processed__10626.txt
        test/aux_files_for_tests/spectra/Rinkitece__R050229__Raman__532__0__unoriented__Raman_Data_Processed__28062.txt
        test/aux_files_for_tests/spectra/Rinkitece__R050229__Raman__780__0__unoriented__Raman_Data_Processed__28061.txt
        test/aux_files_for_tests/spectra/Simonkolleite__R130117__Raman__532__0__unoriented__Raman_Data_Processed__37618.txt
        test/aux_files_for_tests/spectra/Simonkolleite__R130117__Raman__780__0__unoriented__Raman_Data_Processed__37620.txt
        test/aux_files_for_tests/spectra/Simonkolleite__R130616__Raman__532__0__unoriented__Raman_Data_Processed__38051.txt
        test/aux_files_for_tests/spectra/Simonkolleite__R130616__Raman__780__0__unoriented__Raman_Data_Processed__38053.txt
        test/aux_files_for_tests/spectra/Varulite__R060740__Raman__532__0__unoriented__Raman_Data_Processed__12463.txt
        test/aux_files_for_tests/spectra/Varulite__R060740__Raman__785__0__unoriented__Raman_Data_Processed__14523.txt
        >>> print(database.Rs)
        [  200.    200.5   201.  ...,  1199.   1199.5  1200. ]
        >>> data = np.loadtxt(database.files[0],
        ...                   delimiter=database.input.column_delimiter)
        >>> import pylab
        >>> pylab.plot(data[:,0], data[:,1], '-ob',
        ...            database.Rs, database.intensity_matrix[0, :], 'or') # doctest: +ELLIPSIS
        [<matplotlib.lines.Line2D object at 0x...>, <matplotlib.lines.Line2D object at 0x...>]
    """

    def __init__(self, input_file):

        import os

        try:
            assert isinstance(input_file, str)
        except AssertionError:
            print('AssertionError: Argument input_file ({}) sould be of str '
                  'type.'.format(input_file))
            raise

        try:
            assert os.path.isfile(input_file)
        except AssertionError:
            print('AssertionError: The argument input_file ({}) is not an '
                  'existing file.'.format(input_file))
            raise

        self.input_file = input_file
        self.input = None
        self.files = None
        self._data = None  # It is deleted after creation of intensity matrix.
        self.Rs = None
        self.intensity_matrix = None
        self.n_data_points = None
        self.n_spectra = None

    def load(self):
        """Load spectra according to info present in file self.input_file."""

        self.input = InputGeneral(self.input_file)
        self.read_data()
        self.create_Rs()
        self.create_intensity_matrix()
        self.get_n_points_n_spectra()

    def read_data(self):
        """Read spectra into self._data.

        Each item in the list self._data holds the data of a full spectrum and
        is formed by two items, the first being the Raman shift and the second
        being the intensity. The data stored in self._data is later
        transformed into self.intensity_matrix, a numpy.ndarray of size
        n_spectra x n_points, which holds the interpolated intensity of Raman
        spectra.
        """

        import glob

        print('*** Reading data...')
        t0 = time.time()

        # Create sorted list of files with spectrum data.
        self.files = sorted(glob.glob('{}/{}'.format(self.input.directory,
                                                     self.input.file_names)))

        try:
            assert self.files
        except AssertionError:
            print('AssertionError: Did not find any file to load.')
            raise

        # Initialize spectra data.
        self._data = []

        # Read spectra.
        for i, file in enumerate(self.files):
            QtCore.QCoreApplication.processEvents()
            self._data.append([])
            point_to_store = False
            with open(file, mode='r', encoding='utf8') as f:
                for line in f:
                    QtCore.QCoreApplication.processEvents()
                    line = line.strip(' \t\n')
                    if line:
                        if line[0] != '#':  # Not a comment line.
                            fields = line.split(self.input.column_delimiter)
                            while '' in fields:
                                fields.remove('')
                            try:
                                Rs = np.float(fields[0])
                            except ValueError:
                                print('ValueError: It was not possible to '
                                      'convert {} to float when reading line '
                                      '{} in file {}.'.format(
                                          fields[0], line, file))
                                raise
                            try:
                                I = np.float(fields[1])
                            except ValueError:
                                print('ValueError: It was not possible to '
                                      'convert {} to float when reading line '
                                      '{} in file {}.'.format(
                                          fields[1], line, file))
                                raise
                            if Rs <= self.input.Rs_min:
                                # The following values will possibly
                                # constitute the first point in self._data. It
                                # is necessary to store a point with Rs <=
                                # self.input.Rs_min if we want to later carry
                                # out an interpolation (rather than an
                                # extrapolation) in the extreme of lower limit
                                # of analysis.
                                Rs_0 = Rs
                                I_0 = I
                                point_to_store = True
                            else:
                                if point_to_store:
                                    self._data[i].append([Rs_0, I_0])
                                    point_to_store = False
                                if Rs < self.input.Rs_max:
                                    self._data[i].append([Rs, I])
                                else:
                                    # One and only one point with Rs >=
                                    # self.input.Rs_max is needed for a proper
                                    # interpolation in the extreme of the
                                    # upper range of analysis.
                                    self._data[i].append([Rs, I])
                                    break
            try:
                assert len(self._data[i]) > 1
            except AssertionError:
                print('AssertionError: Number of rows in data matrix '
                      'should be larger than 1 in file {}.'.format(file))
                raise
            if i == 0 or (not (i+1)%100) or i == len(self.files)-1:
                print('    {:<6.3f}%'.format((i+1)/len(self.files)*100))

        print('    {:g} seconds for reading data.'.format(time.time() - t0))
        print('*** Finished reading data.\n')

    def create_Rs(self):
        """Create Raman shift values.

        This vector is used as a reference vector for interpolation.
        """

        print('*** Creating Raman shift values...')
        t0 = time.time()

        self.Rs = np.arange(self.input.Rs_min,
                            self.input.Rs_max + self.input.Rs_step,
                            self.input.Rs_step,
                            np.float)

        print('    {:g} seconds for creating Raman shift values.'.format(
            time.time() - t0))
        print('*** Finished creating Raman shift values.\n')

    def create_intensity_matrix(self):
        """Create intensity matrix.

        The data stored in self._data is used for creating
        self.intensity_matrix, a numpy.ndarray of size n_spectra x n_points
        which holds the intensity of Raman spectra after interpolation.
        """

        print('*** Creating intensity matrix...')
        t0 = time.time()

        self.intensity_matrix = np.zeros((len(self.files), len(self.Rs)))

        for spectrum_id, spectrum in enumerate(self._data):

            QtCore.QCoreApplication.processEvents()
            spectrum_array = np.array(spectrum, dtype=np.float)

            # Update row spectrum_id of intensity_matrix.
            try:
                self.intensity_matrix[spectrum_id, :] = \
                        np.interp(self.Rs,
                                  spectrum_array[:, 0],
                                  spectrum_array[:, 1])
            except ValueError:
                print('ValueError: Could not perform interpolation in data '
                      'corresponding to file {}. Please be sure that all '
                      'files contain data inside interpolation: change '
                      'interpolation range or remove this file from '
                      'analysis.'.format(self.files[spectrum_id]))
                raise

        del self._data

        print('    {:g} seconds for creating intensity matrix.'.format(
            time.time() - t0))
        print('*** Finished creating intensity matrix.\n')

    def get_n_points_n_spectra(self):
        """Get number of experimental spectra and number of data points in
        each of them.
        """

        (self.n_spectra, self.n_data_points) = self.intensity_matrix.shape


    def plot_arguments(self, spectrum_id, normalized=False):
        """Return plot info concerning the spectrum identified by
        spectrum_id.

        Args:
            spectrum_id (int): Spectrum index.
            normalized (bool): If True, normalize the spectrum intensity.
        """

        try:
            assert isinstance(spectrum_id, int)
        except AssertionError:
            print('AssertionError: Argument spectrum_id ({}) sould be of int '
                  'type.'.format(spectrum_id))
            raise

        try:
            assert isinstance(normalized, bool)
        except AssertionError:
            print('AssertionError: Argument normalized ({}) sould be of bool '
                  'type.'.format(normalized))
            raise

        Rs = self.Rs
        I = self.intensity_matrix[spectrum_id, :]
        name = self.files[spectrum_id].split('/')[-1]
        if normalized:
            I = I/max(I)
        arguments = {'x': Rs,
                     'y': I,
                     'color': 'steelblue',
                     'label': name}
        return arguments

    def plot(self, spectrum_id, normalized=False):
        """Plot the spectrum identified by spectrum_id.

        Args:
            spectrum_id (int): Spectrum index.
            normalized (bool): If True, normalize the spectrum intensity.
        """

        import matplotlib.pyplot as plt

        try:
            assert isinstance(spectrum_id, int)
        except AssertionError:
            print('AssertionError: Argument spectrum_id ({}) sould be of int '
                  'type.'.format(spectrum_id))
            raise

        try:
            assert isinstance(normalized, bool)
        except AssertionError:
            print('AssertionError: Argument normalized ({}) sould be of bool '
                  'type.'.format(normalized))
            raise

        plt.figure()
        plt.xlabel('Raman shift')
        if normalized:
            plt.ylabel('Normalized intensity')
        else:
            plt.ylabel('Intensity')
        plot_arguments = self.plot_arguments(spectrum_id, normalized)
        plot_from_args(plot_arguments)

    def plot_from_name(self, filename, normalized=False):
        """Plot the spectrum whose name is filename.

        Args:
            filename (str): File name.
            normalized (bool): If True, normalize the spectrum intensity.
        """

        try:
            assert isinstance(filename, str)
        except AssertionError:
            print('AssertionError: Argument filename ({}) sould be of str '
                  'type.'.format(filename))
            raise

        try:
            assert isinstance(normalized, bool)
        except AssertionError:
            print('AssertionError: Argument normalized ({}) sould be of bool '
                  'type.'.format(normalized))
            raise

        for file_id, file in enumerate(self.files):
            if file == filename or file.split('/')[-1] == filename:
                self.plot(file_id, normalized)
                break
        else:
            print('ValueError: Argument filename ({}) does not correspond to '
                  'any of the loaded files.'.format(filename))
            raise ValueError

class SpectraExperiment(SpectraGeneral):
    """Handle experimental Raman spectra.

    Args:
        R_D (rpy2.robjects.vectors.Matrix): Intensity matrix in R format.
        (Others): See parent class.
    """

    def __init__(self, input_file):

        super().__init__(input_file)
        self.R_D = None

    def load(self):
        """Load experimental spectra and set self.R_D."""

        super().load()
        self.create_R_D()

    def create_R_D(self):
        """Convert self.intensity_matrix for R software format."""

        r = robjects.r

        numpy2ri.activate()
        self.R_D = r.matrix(self.intensity_matrix, nrow=self.n_spectra,
                            ncol=self.n_data_points)
        numpy2ri.deactivate()


class SpectraDatabase(SpectraGeneral):
    """Handle database Raman spectra.

    Args:
        chemical_elements (list of list): List of list; each sublist contains
            the elements present in the ideal chemical composition of the
            corresponding database entry. It is currently designed to be used
            with RRUFF database. If the database entry has not the ideal
            chemical composition defined, then the corresponding list item is
            set to None and that entry is not filtered out before search-match
            process.
        (Others): See parent class.

    Warning:
        It is currently designed to be used with RRUFF database.

    Example:
        >>> database = SpectraDatabase('test/aux_files_for_tests/'
        ...     'spectra/Raman_database.rpirs')
        >>> database.load() # doctest: +ELLIPSIS
        *** Reading input file...
            directory = test/aux_files_for_tests/spectra
            file_names = *.txt
            column_delimiter = ,
            Rs_min = 200.0
            Rs_max = 1200.0
            Rs_step = 0.5
        *** Finished reading input file.
        <BLANKLINE>
        *** Reading data...
            2.857 %
            100.000%
            ... seconds for reading data.
        *** Finished reading data.
        <BLANKLINE>
        *** Creating Raman shift values...
            ... seconds for creating Raman shift values.
        *** Finished creating Raman shift values.
        <BLANKLINE>
        *** Creating intensity matrix...
            ... seconds for creating intensity matrix.
        *** Finished creating intensity matrix.
        <BLANKLINE>
        *** Reading chemical elements associated to database entries...
            ... seconds for reading chemical elements associated to database entries.
        *** Finished reading chemical elements associated to database entries.
        <BLANKLINE>
        >>> print(database.chemical_elements) # doctest: +ELLIPSIS
        [['C', 'H', 'N', 'Ni'], ['C', 'H', 'N', 'Ni'], ['Cl', 'H', 'O', 'Sn'], ..., ['Ca', 'Mn', 'Na', 'O', 'P']]
    """

    def __init__(self, input_file):

        super().__init__(input_file)
        self.chemical_elements = None

    def load(self):
        """Load database spectra and set self.chemical_elements."""

        super().load()
        self.get_chemical_elements()

    def get_chemical_elements(self):
        """Get the chemical elements of each database entry and put them in a
        list.

        Warning:
            The extraction of chemical elements from text files is currently
            designed to be used with RRUFF database.
        """

        self.chemical_elements = []

        print('*** Reading chemical elements associated to database '
              'entries...')
        t0 = time.time()

        for file in self.files:
            QtCore.QCoreApplication.processEvents()
            with open(file, mode='r', encoding='utf8') as f:
                lines = f.readlines()
                for line in lines:
                    QtCore.QCoreApplication.processEvents()
                    line = line.strip(' \t\n')
                    if line[0] == '#':
                        if 'IDEAL CHEMISTRY' in line:
                            elements = get_chemical_elements_in_line(line)
                            self.chemical_elements.append(elements)
                            break
                    else:
                        self.chemical_elements.append(None)
                        print("WARNING: Reached end of comment lines in file "
                              "{} without finding the expression 'IDEAL "
                              "CHEMISTRY'. This file won't be filtered out "
                              "by chemical composition.".format(file))
                        break

        print('    {:g} seconds for reading chemical elements associated to '
              'database entries.'.format(time.time() - t0))
        print('*** Finished reading chemical elements associated to database '
              'entries.\n')

class InputMCR(object):
    """Handle MCR input file.

    Args:
        input_file (str): File (with full path) containing info about the
            other attributes in this class.
        baseline_fitting (bool): Define if baseline should be fitted
            to experimental spectra, and subtracted from it, before MCR
            analysis.
        baseline_fitting_lambda (float): Value of lambda in algorithm for
            baseline fitting.
        baseline_fitting_p (float): Value of p in algorithm for baseline
            fitting.
        baseline_fitting_tol (float): Relative tolerance in weight for
            convergence in algorithm for baseline fitting to be achieved (see
            function MCR.baseline_als).
        baseline_fitting_max_iter (int) Maximum number of iterations in
            algorithm for baseline fitting.
        intensity_filter (bool): Define if intensity filter should be applied
            to database before search-match process.
        intensity_value (float): If self.intensity_filter is True, then
            experimental spectra in which all intensities are greater than
            intensity_value will be removed before MCR analysis.
        number_of_components (int): Specify how many component spectra the
            MCR analysis must find.
        use_thresh (bool): Define if thresh argument in call to als function
            of ALS package should be used. According to ALS manual,  thresh is
            a "numeric value that defaults to .001; if ((oldrss - rss) /
            oldrss) < thresh then the optimization stops, where oldrss is the
            residual sum of squares  at iteration x-1 and rss is the residual
            sum of squares at iteration x".
        thresh_value (float): Value of argument thresh in call to als
            function. It is indeed used only if use_thresh is True.
        use_maxiter (bool): Define if maxiter argument in call to als function
            of ALS package should be used. According to ALS manual, maxiter
            is "the maximum number of iterations to perform (where an
            iteration is optimization of either AList and C)".
        maxiter_value (int): Value of argument maxiter in call to als
            function. It is indeed used only if use_maxiter is True.
        use_forcemaxiter (bool): Define if forcemaxiter argument in call to
            als function of ALS package should be used. According to ALS
            manual, forcemaxiter is a "logical indicating whether maxiter
            iterations should be performed even if the residual difference
            drops below thresh."
        forcemaxiter_value (bool): Value of argument forcemaxiter in call to
            als function. It is indeed used only if use_forcemaxiter is True.
        use_optS1st (bool): Define if optS1st argument in call to als function
            of ALS package should be used. According to ALS manual, optS1st is
            a "logical indicating whether the first constrained least squares
            regression should estimate S or CList". (In current
            implementation of this module, CList contains only 1 item.)
        optS1st_value (bool): Value of argument optS1st in call to als
            function. It is indeed used only if use_optS1st is True.
        use_baseline (bool): Define if baseline argument in call to als
            function of ALS package should be used. According to ALS manual,
            baseline is a "logical indicating whether a baseline component is
            present; if baseline=TRUE then this component is exempt from
            constraints unimodality or non-negativity".
        baseline_value (bool): Value of argument baseline in call to als
            function. It is indeed used only if use_baseline is True.
        use_nonnegS (bool): Define if nonnegS argument in call to als function
            of ALS package should be used. According to ALS manual, nonnegS is
            a "logical indicating whether the components (columns) of the
            matrix S should be constrained to non-negative values".
        nonnegS_value (bool): Value of argument nonnegS in call to als
            function. It is indeed used only if use_nonnegS is True.
        use_nonnegC (bool): Define if nonnegC argument in call to als function
            of ALS package should be used. According to ALS manual, nonnegC is
            a "logical indicating whether the components (columns) of the
            matrix C should be constrained to non-negative values".
        nonnegC_value (bool): Value of argument nonnegC in call to als
            function. It is indeed used only if use_nonnegC is True.
        use_uniS (bool): Define if uniS argument in call to als function of
            ALS package should be used. According to ALS manual, uniS is a
            "logical indicating whether unimodality constraints should be
            applied to the columns of S".
        uniS_value (bool): Value of argument uniS in call to als function. It
            is indeed used only if use_uniS is True.
        use_uniC (bool): Define if uniC argument in call to als function of
            ALS package should be used. According to ALS manual, uniC is a
            "logical indicating whether unimodality constraints should be
            applied to the columns of C".
        uniC_value (bool): Value of argument uniC in call to als function. It
            is indeed used only if use_uniC is True.
        use_normS (bool): Define if normS argument in call to als function of
            ALS package should be used. According to ALS manual, normS is a
            "numeric indicating whether the spectra are normalized; if
            normS>0, the spectra are normalized. If normS==1 the maximum of
            the spectrum of each component is constrained to be equal to one;
            if normS>0 && normS!=1 then the norm of the spectrum of each
            component is constrained to be equal to one".
        normS_value (int): Value of argument normS in call to als function.
            It is indeed used only if use_normS is True.
        chemical_filter (bool): Define if chemical filter should be applied to
            database before search-match process.
        chemical_elements (str list): List of elements present in the sample,
            which is used to filter the database in search-match process if
            chemical_filter is True.
        laser_filter (bool): Define if laser filter should be applied to
            database before search-match process. The filtering is currently
            performed by searching, in the database entry filename, the string
            corresponding to '__<laser_wavelength>__', where
            <laser_wavelength> is defined in self.input.laser_wavelength.
        laser_wavelength (int): Laser wavelength, which may be used to filter
            the database in search-match process. The filtering is currently
            performed by searching, in the database entry filename, the string
            corresponding to '__<laser_wavelength>__'.

    Example:
        >>> input = InputMCR('test/aux_files_for_tests/Raman_mcr.rpirs')
        *** Reading input file...
            number_of_components = 2
            baseline_fitting = True
            baseline_fitting_lambda = 10000000.0
            baseline_fitting_p = 0.05
            baseline_fitting_tol = 0.001
            baseline_fitting_max_iter = 10
            intensity_filter = False
            intensity_value = 1000.0
            chemical_filter = False
            chemical_elements = ['Al', 'Ca', 'Fe', 'H', 'K', 'Mg', 'Mn', 'Na', 'O', 'P', 'Si', 'Ti']
            laser_filter = False
            laser_wavelength = 514
            use_thresh = False
            thresh_value = 0.001
            use_maxiter = False
            maxiter_value = 100
            use_forcemaxiter = False
            forcemaxiter_value = True
            use_optS1st = False
            optS1st_value = True
            use_baseline = False
            baseline_value = False
            use_nonnegS = False
            nonnegS_value = True
            use_nonnegC = False
            nonnegC_value = True
            use_uniS = False
            uniS_value = False
            use_uniC = False
            uniC_value = False
            use_normS = False
            normS_value = 0
        *** Finished reading input file.
        <BLANKLINE>

    Warning:
        The database filtering by laser wavelength is currently performed by
        searching, in the database entry filename, the string corresponding to
        '__<laser_wavelength>__', where <laser_wavelength> is defined in
        self.input.laser_wavelength.
    """

    def __init__(self, input_file):

        import os

        try:
            assert isinstance(input_file, str)
        except AssertionError:
            print('AssertionError: Argument input_file ({}) sould be of str '
                  'type.'.format(input_file))
            raise

        try:
            assert os.path.isfile(input_file)
        except AssertionError:
            print('AssertionError: The argument input_file ({}) is not an '
                  'existing file.'.format(input_file))
            raise

        self.input_file = input_file
        self.number_of_components = None
        self.baseline_fitting = None
        self.baseline_fitting_lambda = None
        self.baseline_fitting_p = None
        self.baseline_fitting_tol = None
        self.baseline_fitting_max_iter = None
        self.intensity_filter = None
        self.intensity_value = None
        self.chemical_filter = None
        self.chemical_elements = None
        self.laser_filter = None
        self.laser_wavelength = None
        self.use_thresh = None
        self.thresh_value = None
        self.use_maxiter = None
        self.maxiter_value = None
        self.use_forcemaxiter = None
        self.forcemaxiter_value = None
        self.use_optS1st = None
        self.optS1st_value = None
        self.use_baseline = None
        self.baseline_value = None
        self.use_nonnegS = None
        self.nonnegS_value = None
        self.use_nonnegC = None
        self.nonnegC_value = None
        self.use_uniS = None
        self.uniS_value = None
        self.use_uniC = None
        self.uniC_value = None
        self.use_normS = None
        self.normS_value = None

        self.read_input_file()

    def read_input_file(self):
        """Read input file for MCR analysis."""

        elements_NIST = \
            ['H', 'He',
             'Li', 'Be', 'B', 'C', 'N', 'O', 'F', 'Ne',
             'Na', 'Mg', 'Al', 'Si', 'P', 'S', 'Cl', 'Ar',
             'K', 'Ca', 'Sc', 'Ti', 'V', 'Cr', 'Mn', 'Fe', 'Co', 'Ni', 'Cu',
             'Zn', 'Ga', 'Ge', 'As', 'Se', 'Br', 'Kr',
             'Rb', 'Sr', 'Y', 'Zr', 'Nb', 'Mo', 'Tc', 'Ru', 'Rh', 'Pd', 'Ag',
             'Cd', 'In', 'Sn', 'Sb', 'Te', 'I', 'Xe',
             'Cs', 'Ba', 'La', 'Ce', 'Pr', 'Nd', 'Pm', 'Sm', 'Eu', 'Gd', 'Tb',
             'Dy', 'Ho', 'Er', 'Tm', 'Yb', 'Lu', 'Hf', 'Ta', 'W', 'Re',
             'Os', 'Ir', 'Pt', 'Au', 'Hg', 'Tl', 'Pb', 'Bi', 'Po', 'At',
             'Rn',
             'Fr', 'Ra', 'Ac', 'Th', 'Pa', 'U', 'Np', 'Pu', 'Am', 'Cm', 'Bk',
             'Cf', 'Es', 'Fm', 'Md', 'No', 'Lr', 'Rf', 'Db', 'Sg', 'Bh',
             'Hs', 'Mt', 'Ds', 'Rg', 'Cn', 'Nh', 'Fl', 'Mc', 'Lv', 'Ts',
             'Og']

        print('*** Reading input file...')

        tags_in_file = {'# number_of_components': False,
                        '# baseline_fitting': False,
                        '# baseline_fitting_lambda': False,
                        '# baseline_fitting_p': False,
                        '# baseline_fitting_tol': False,
                        '# baseline_fitting_max_iter': False,
                        '# intensity_filter': False,
                        '# intensity_value': False,
                        '# chemical_filter': False,
                        '# chemical_elements': False,
                        '# laser_filter': False,
                        '# laser_wavelength': False,
                        '# use_thresh': False,
                        '# thresh_value': False,
                        '# use_maxiter': False,
                        '# maxiter_value': False,
                        '# use_forcemaxiter': False,
                        '# forcemaxiter_value': False,
                        '# use_optS1st': False,
                        '# optS1st_value': False,
                        '# use_baseline': False,
                        '# baseline_value': False,
                        '# use_nonnegS': False,
                        '# nonnegS_value': False,
                        '# use_nonnegC': False,
                        '# nonnegC_value': False,
                        '# use_uniS': False,
                        '# uniS_value': False,
                        '# use_uniC': False,
                        '# uniC_value': False,
                        '# use_normS': False,
                        '# normS_value': False}

        with open(self.input_file, mode='r', encoding='utf8') as f:

            lines = f.readlines()

            for line_id, line in enumerate(lines):

                QtCore.QCoreApplication.processEvents()
                line_stripped = line.strip(' \t\n')

                if line_id < len(lines)-1:
                    next_line_stripped = lines[line_id+1].strip(' \t\n')

                if line_stripped == '# number_of_components':
                    number_of_components = np.int(next_line_stripped)
                    print('    number_of_components = {}'.format(
                        number_of_components))
                    tags_in_file['# number_of_components'] = True

                elif line_stripped == '# baseline_fitting':
                    baseline_fitting = str_to_bool(next_line_stripped)
                    print('    baseline_fitting = {}'.format(
                        baseline_fitting))
                    tags_in_file['# baseline_fitting'] = True

                elif line_stripped == '# baseline_fitting_lambda':
                    baseline_fitting_lambda = np.float(next_line_stripped)
                    print('    baseline_fitting_lambda = {}'.format(
                        baseline_fitting_lambda))
                    tags_in_file['# baseline_fitting_lambda'] = True

                elif line_stripped == '# baseline_fitting_p':
                    baseline_fitting_p = np.float(next_line_stripped)
                    print('    baseline_fitting_p = {}'.format(
                        baseline_fitting_p))
                    tags_in_file['# baseline_fitting_p'] = True

                elif line_stripped == '# baseline_fitting_tol':
                    baseline_fitting_tol = np.float(next_line_stripped)
                    print('    baseline_fitting_tol = {}'.format(
                        baseline_fitting_tol))
                    tags_in_file['# baseline_fitting_tol'] = True

                elif line_stripped == '# baseline_fitting_max_iter':
                    baseline_fitting_max_iter = np.int(next_line_stripped)
                    print('    baseline_fitting_max_iter = {}'.format(
                        baseline_fitting_max_iter))
                    tags_in_file['# baseline_fitting_max_iter'] = True

                elif line_stripped == '# intensity_filter':
                    intensity_filter = str_to_bool(next_line_stripped)
                    print('    intensity_filter = {}'.format(
                        intensity_filter))
                    tags_in_file['# intensity_filter'] = True

                elif line_stripped == '# intensity_value':
                    intensity_value = np.float(next_line_stripped)
                    print('    intensity_value = {}'.format(intensity_value))
                    tags_in_file['# intensity_value'] = True

                elif line_stripped == '# chemical_filter':
                    chemical_filter = str_to_bool(next_line_stripped)
                    print('    chemical_filter = {}'.format(chemical_filter))
                    tags_in_file['# chemical_filter'] = True

                elif line_stripped == '# chemical_elements':
                    chemical_elements = next_line_stripped.split(' ')
                    while '' in chemical_elements:
                        chemical_elements.remove('')
                    print('    chemical_elements = {}'.format(
                        chemical_elements))
                    tags_in_file['# chemical_elements'] = True
                    for element in chemical_elements:
                        try:
                            assert element in elements_NIST
                        except AssertionError:
                            print('AssertionError: Element {} is not '
                                  'valid.'.format(element))
                            raise

                elif line_stripped == '# laser_filter':
                    laser_filter = str_to_bool(next_line_stripped)
                    print('    laser_filter = {}'.format(laser_filter))
                    tags_in_file['# laser_filter'] = True

                elif line_stripped == '# laser_wavelength':
                    laser_wavelength = np.int(next_line_stripped)
                    print('    laser_wavelength = {}'.format(laser_wavelength))
                    tags_in_file['# laser_wavelength'] = True

                elif line_stripped == '# use_thresh':
                    use_thresh = str_to_bool(next_line_stripped)
                    print('    use_thresh = {}'.format(use_thresh))
                    tags_in_file['# use_thresh'] = True

                elif line_stripped == '# thresh_value':
                    thresh_value = np.float(next_line_stripped)
                    print('    thresh_value = {}'.format(thresh_value))
                    tags_in_file['# thresh_value'] = True

                elif line_stripped == '# use_maxiter':
                    use_maxiter = str_to_bool(next_line_stripped)
                    print('    use_maxiter = {}'.format(use_maxiter))
                    tags_in_file['# use_maxiter'] = True

                elif line_stripped == '# maxiter_value':
                    maxiter_value = np.int(next_line_stripped)
                    print('    maxiter_value = {}'.format(maxiter_value))
                    tags_in_file['# maxiter_value'] = True

                elif line_stripped == '# use_forcemaxiter':
                    use_forcemaxiter = str_to_bool(next_line_stripped)
                    print('    use_forcemaxiter = {}'.format(
                        use_forcemaxiter))
                    tags_in_file['# use_forcemaxiter'] = True

                elif line_stripped == '# forcemaxiter_value':
                    forcemaxiter_value = str_to_bool(next_line_stripped)
                    print('    forcemaxiter_value = {}'.format(
                        forcemaxiter_value))
                    tags_in_file['# forcemaxiter_value'] = True

                elif line_stripped == '# use_optS1st':
                    use_optS1st = str_to_bool(next_line_stripped)
                    print('    use_optS1st = {}'.format(use_optS1st))
                    tags_in_file['# use_optS1st'] = True

                elif line_stripped == '# optS1st_value':
                    optS1st_value = str_to_bool(next_line_stripped)
                    print('    optS1st_value = {}'.format(optS1st_value))
                    tags_in_file['# optS1st_value'] = True

                elif line_stripped == '# use_baseline':
                    use_baseline = str_to_bool(next_line_stripped)
                    print('    use_baseline = {}'.format(use_baseline))
                    tags_in_file['# use_baseline'] = True

                elif line_stripped == '# baseline_value':
                    baseline_value = str_to_bool(next_line_stripped)
                    print('    baseline_value = {}'.format(baseline_value))
                    tags_in_file['# baseline_value'] = True

                elif line_stripped == '# use_nonnegS':
                    use_nonnegS = str_to_bool(next_line_stripped)
                    print('    use_nonnegS = {}'.format(use_nonnegS))
                    tags_in_file['# use_nonnegS'] = True

                elif line_stripped == '# nonnegS_value':
                    nonnegS_value = str_to_bool(next_line_stripped)
                    print('    nonnegS_value = {}'.format(nonnegS_value))
                    tags_in_file['# nonnegS_value'] = True

                elif line_stripped == '# use_nonnegC':
                    use_nonnegC = str_to_bool(next_line_stripped)
                    print('    use_nonnegC = {}'.format(use_nonnegC))
                    tags_in_file['# use_nonnegC'] = True

                elif line_stripped == '# nonnegC_value':
                    nonnegC_value = str_to_bool(next_line_stripped)
                    print('    nonnegC_value = {}'.format(nonnegC_value))
                    tags_in_file['# nonnegC_value'] = True

                elif line_stripped == '# use_uniS':
                    use_uniS = str_to_bool(next_line_stripped)
                    print('    use_uniS = {}'.format(use_uniS))
                    tags_in_file['# use_uniS'] = True

                elif line_stripped == '# uniS_value':
                    uniS_value = str_to_bool(next_line_stripped)
                    print('    uniS_value = {}'.format(uniS_value))
                    tags_in_file['# uniS_value'] = True

                elif line_stripped == '# use_uniC':
                    use_uniC = str_to_bool(next_line_stripped)
                    print('    use_uniC = {}'.format(use_uniC))
                    tags_in_file['# use_uniC'] = True

                elif line_stripped == '# uniC_value':
                    uniC_value = str_to_bool(next_line_stripped)
                    print('    uniC_value = {}'.format(uniC_value))
                    tags_in_file['# uniC_value'] = True

                elif line_stripped == '# use_normS':
                    use_normS = str_to_bool(next_line_stripped)
                    print('    use_normS = {}'.format(use_normS))
                    tags_in_file['# use_normS'] = True

                elif line_stripped == '# normS_value':
                    normS_value = np.int(next_line_stripped)
                    print('    normS_value = {}'.format(normS_value))
                    tags_in_file['# normS_value'] = True

        for key in tags_in_file:
            try:
                assert tags_in_file[key]
            except AssertionError:
                print('AssertionError: Tag "{}" is not present in input '
                      'file.'.format(key))
                raise

        self.number_of_components = number_of_components
        self.baseline_fitting = baseline_fitting
        self.baseline_fitting_lambda = baseline_fitting_lambda
        self.baseline_fitting_p = baseline_fitting_p
        self.baseline_fitting_tol = baseline_fitting_tol
        self.baseline_fitting_max_iter = baseline_fitting_max_iter
        self.intensity_filter = intensity_filter
        self.intensity_value = intensity_value
        self.chemical_filter = chemical_filter
        self.chemical_elements = chemical_elements
        self.laser_filter = laser_filter
        self.laser_wavelength = laser_wavelength
        self.use_thresh = use_thresh
        self.thresh_value = thresh_value
        self.use_maxiter = use_maxiter
        self.maxiter_value = maxiter_value
        self.use_forcemaxiter = use_forcemaxiter
        self.forcemaxiter_value = forcemaxiter_value
        self.use_optS1st = use_optS1st
        self.optS1st_value = optS1st_value
        self.use_baseline = use_baseline
        self.baseline_value = baseline_value
        self.use_nonnegS = use_nonnegS
        self.nonnegS_value = nonnegS_value
        self.use_nonnegC = use_nonnegC
        self.nonnegC_value = nonnegC_value
        self.use_uniS = use_uniS
        self.uniS_value = uniS_value
        self.use_uniC = use_uniC
        self.uniC_value = uniC_value
        self.use_normS = use_normS
        self.normS_value = normS_value

        print('*** Finished reading input file.\n')


class Candidate(object):
    """Handle candidate information in search-match process.

    Args:
        name (str): Name of the candidate phase.
        database_id (int): Index of the candidate in database.
        match_factor (float): Match factor as evaluated by matchFactor
            function of ALS package.
    """

    def __init__(self, name, database_id, match_factor):

        try:
            assert isinstance(name, str)
        except AssertionError:
            print('AssertionError: Argument name ({}) sould be of str '
                  'type.'.format(name))
            raise

        try:
            assert name
        except AssertionError:
            print('AssertionError: Argument name must not have zero length.')
            raise

        try:
            assert isinstance(database_id, int)
        except AssertionError:
            print('AssertionError: Argument database_id ({}) should be of '
                  'int type.'.format(database_id))
            raise

        try:
            assert isinstance(match_factor, float) or \
                isinstance(match_factor, int)
        except AssertionError:
            print('AssertionError: Argument match_factor ({}) should be of '
                  'float or int type.'.format(match_factor))
            raise

        self.name = name
        self.database_id = database_id
        self.match_factor = match_factor


class Candidates(object):
    """Store canditate list and matched candidate for a given component
    spectrum.

    Args:
        candidate_list (list of Candidate instances): List of Candidate
            instances.
        matched (int): The candidate which is chosen as the one which best
            matches the component spectrum, in the user opinion.

    Example:
        >>> candidates = Candidates()
        >>> dictionary = {'A': (0, 0.99),
        ...               'B': (1, 0.88),
        ...               'C': (2, 0.95),
        ...               'D': (3, 0.00),
        ...               'E': (4, 1.00)}
        >>> names = ['A', 'B', 'C', 'D', 'E']
        >>> candidates.candidate_list = [Candidate(names[i],
        ...                                        dictionary[names[i]][0],
        ...                                        dictionary[names[i]][1])
        ...                              for i in range(5)]
        >>> candidates.sort()
        >>> for i, candidate in enumerate(candidates.candidate_list):
        ...     print(candidate.name,
        ...           candidate.database_id,
        ...           candidate.match_factor)
        E 4 1.0
        A 0 0.99
        C 2 0.95
        B 1 0.88
        D 3 0.0
    """

    def __init__(self):
        self.candidate_list = []
        self.matched = None

    def sort(self):
        """Sort candidates in self.candidate_list from higher to lower match
        factor.
        """

        match_factors = [self.candidate_list[i].match_factor for i
                         in range(len(self.candidate_list))]
        indices = sorted(range(len(match_factors)),
                         key=lambda k: match_factors[k], reverse=True)
        self.candidate_list = [self.candidate_list[i] for i in indices]

class MCR(object):
    """Handle MCR analysis and match the component spectra found against a
    database.

    Args:
        input_file (str): File (with full path) containing info about the
            MCR analysis and search-match against the database.
        experiment (SpectraExperiment instance): A SpectraExperiment instance
            containing info the about the experimental Raman spectra.
        database (SpectraDatabase instance): A SpectraDatabase instance
            containing info the about the database Raman spectra.
        input (InputMCR instance): A InputMCR instance created from info
            contained in file input_file.
        intensity_indices (int list): List with indices of experimental
            spectra for which all intensities are greater than the value
            defined in self.input.intensity_value.
        experiment_handled (SpectraExperiment instance): Copy of experiment.
            Fitted baseline is subtracted if self.input.baseline_fitting is
            True. All spectra for which all intensities are greater than
            self.input.intensity_value are removed if
            self.input.intensity_filter is True. This object is used for MCR
            analysis.
        database_filtered_id (int list): List corresponding to indices of
            database spectra to be considered in search-match process. If a
            index is not contained in this list, it is because it has been
            filtered out.
        S (numpy.ndarray): Matrix in which each column holds the intensity
            values of a component spectrum.
        S0_seed (tuple): Tuple containing info about the internal state of
            random number generator immediately before the generation of the
            initial (random) guess for matrix S. Then, this initial guess can
            be regenerated, if needed.
        C (numpy.ndarray): Matrix containing the coefficients of the linear
            combination of component spectra which approximate the
            experimental spectra.
        C0_seed (tuple): Tuple containing info about the internal state of
            random number generator immediately before the generation of the
            initial (random) guess for matrix C. Then, this initial guess can
            be regenerated, if needed.
        RSS (float): The residual sum of squares at termination of MCR
            analysis.
        best_matches (list of Candidates instances): List where each item is
            a Candidates instance containing info about the search-match
            process for the corresponding component spectrum.
    """

    def __init__(self, input_file, experiment, database):

        import os

        try:
            assert isinstance(input_file, str)
        except AssertionError:
            print('AssertionError: Argument input_file ({}) sould be of str '
                  'type.'.format(input_file))
            raise

        try:
            assert os.path.isfile(input_file)
        except AssertionError:
            print('AssertionError: The argument input_file ({}) is not an '
                  'existing file.'.format(input_file))
            raise

        try:
            assert isinstance(experiment, SpectraExperiment)
        except AssertionError:
            print("AssertionError: Argument experiment ({}) must be of type "
                  "<class 'rpirs.SpectraExperiment'>.".format(experiment))
            raise

        try:
            assert isinstance(database, SpectraDatabase)
        except AssertionError:
            print("AssertionError: Argument database ({}) must be of type "
                  "<class 'rpirs.SpectraDatabase'>.".format(database))
            raise

        self.input_file = input_file
        self.experiment = experiment
        self.database = database
        self.input = None
        self.intensity_indices = None
        self.experiment_handled = None
        self.database_filtered_id = None
        self.S = None
        self.S0_seed = None
        self.C = None
        self.C0_seed = None
        self.RSS = None
        self.best_matches = None

    def find_intensity_indices(self):
        """Find indices of self.experiment.intensity_matrix.

        If self.input.intensity_filter is True, find indices corresponding
        to experimental spectra in which all intensities are greater than the
        value defined in self.input.intensity_value.
        """

        self.intensity_indices = []
        if self.input.intensity_filter:
            for spectrum_id in range(self.experiment.n_spectra):
                QtCore.QCoreApplication.processEvents()
                if (np.min(self.experiment.intensity_matrix[spectrum_id, :]) >
                        self.input.intensity_value):
                    self.intensity_indices.append(spectrum_id)

    def handle_experiment(self):
        """Handle experimental Raman spectra.

        Create a copy of experiment. Fitted baseline is subtracted if
        self.input.baseline_fitting is True. All spectra for which all
        intensities are greater than self.input.intensity_value are removed if
        self.input.intensity_filter is True.

        Note:
            The documentation of copy.deepcopy says that "This module does not
            copy types like module, method, stack trace, stack frame, file,
            socket, window, array, or any similar types.". However, the id of
            the intensity matrix (a numpy array) attribute of self.experiment
            and self.experiment_handled (see code below) are different, as
            wished. Also, there are several evidences that the code behavior
            regarding this copy is ok.
        """

        import copy

        self.experiment_handled = copy.deepcopy(self.experiment)

        if self.input.baseline_fitting:
            # Refresh intensity_matrix by subtracting fitted baseline:
            print('*** Subtracting baseline...')
            t0 = time.time()
            for spectrum_id in range(self.experiment.n_spectra):
                QtCore.QCoreApplication.processEvents()
                self.experiment_handled.intensity_matrix[spectrum_id, :] = \
                    self.experiment.intensity_matrix[spectrum_id, :] - \
                    baseline_als(
                        self.experiment.intensity_matrix[spectrum_id, :],
                        self.input.baseline_fitting_lambda,
                        self.input.baseline_fitting_p,
                        self.input.baseline_fitting_tol,
                        self.input.baseline_fitting_max_iter)
                if spectrum_id == 0 or (not (spectrum_id + 1)%100) or \
                        spectrum_id == self.experiment.n_spectra-1:
                    print('    Subtracting baseline: {:<6.3f}%'.format(
                        (spectrum_id+1)/self.experiment.n_spectra*100))
            print('    {:g} seconds for subtracting baseline.'.format(
                time.time() - t0))
            print('*** Finished subtracting baseline.\n')

        print('*** Removing spectra with very high intensity...')
        t0 = time.time()

        self.find_intensity_indices()

        # Refresh intensity_matrix by removing those spectra in which all
        # intensities are greater than self.input.intensity_value:
        self.experiment_handled.intensity_matrix = \
            np.delete(self.experiment_handled.intensity_matrix,
                      self.intensity_indices,
                      axis=0)

        # Refresh attribute files:
        for i in sorted(self.intensity_indices, reverse=True):
            del self.experiment_handled.files[i]

        # Refresh attributes n_spectra and n_data_points:
        (self.experiment_handled.n_spectra,
         self.experiment_handled.n_data_points) = \
         self.experiment_handled.intensity_matrix.shape

        print('    {:g} seconds for removing spectra with very high '
              'intensity.'.format(time.time() - t0))
        print('*** Finished removing spectra with very high intensity.\n')

        # Refresh attribute R_D:
        self.experiment_handled.create_R_D()

    def filter_database(self):
        """Filter database.

        Filter database taking chemical composition and Raman laser wavelength
        into account.

        Todo:
            Test code in test file.
        """

        print('*** Filtering database...')
        t0 = time.time()
        self.database_filtered_id = []
        for spectrum_id, file in enumerate(self.database.files):
            QtCore.QCoreApplication.processEvents()
            # Before the filtering process, all spectra are taken as valid.
            valid_spectrum = True
            if self.input.laser_filter:
                # Test laser wavelength consistency.
                if '__{:d}__'.format(self.input.laser_wavelength) not in file:
                    valid_spectrum = False
            if self.input.chemical_filter:
                chemical_elements = \
                    self.database.chemical_elements[spectrum_id]
                if chemical_elements is not None:
                    # Test chemical composition consistency.
                    for chemical_element in chemical_elements:
                        QtCore.QCoreApplication.processEvents()
                        if chemical_element not in \
                                self.input.chemical_elements:
                            valid_spectrum = False
            if valid_spectrum:
                self.database_filtered_id.append(spectrum_id)
        print('    {:g} seconds for filtering database.'.format(
            time.time() - t0))
        print('*** Finished filtering database.\n')

    def run(self):
        """Perform MCR analysis by calling als function of ALS package from
        software R.
        """

        import random

        r = robjects.r
        ALS = importr('ALS')
        ALSals = r['als']

        self.input = InputMCR(self.input_file)
        self.handle_experiment()

        # Create initial guess for matrix S by using random numbers with
        # uniform distribution in the semi-open interval [0, 1).
        S0 = []
        self.S0_seed = random.getstate()

        for spectrum_id in range(self.experiment_handled.n_data_points):
            QtCore.QCoreApplication.processEvents()
            S0.append([random.random() for base_index in
                       range(self.input.number_of_components)])

        # Transform list into array.
        S0 = np.array(S0, dtype=np.float)

        # Convert matrix for R software format.
        numpy2ri.activate()
        R_S0 = r.matrix(S0,
                        nrow=self.experiment_handled.n_data_points,
                        ncol=self.input.number_of_components)
        numpy2ri.deactivate()

        # Create initial guess for matrix C by using random numbers with
        # uniform distribution in the semi-open interval [0, 1).
        C0 = []
        self.C0_seed = random.getstate()

        for spectrum_id in range(self.experiment_handled.n_spectra):
            QtCore.QCoreApplication.processEvents()
            C0.append([random.random() for base_index in
                       range(self.input.number_of_components)])

        # Transform list into array.
        C0 = np.array(C0, dtype=np.float)

        # Convert matrix for R software format.
        numpy2ri.activate()
        R_C0 = r.matrix(C0,
                        nrow=self.experiment_handled.n_spectra,
                        ncol=self.input.number_of_components)
        numpy2ri.deactivate()

        als_arguments = ('CList=r.list(R_C0), S=R_S0, '
                         'PsiList=r.list(self.experiment_handled.R_D)')

        if self.input.use_thresh:
            als_arguments += ', thresh={}'.format(self.input.thresh_value)

        if self.input.use_maxiter:
            als_arguments += ', maxiter={}'.format(self.input.maxiter_value)

        if self.input.use_forcemaxiter:
            als_arguments += ', forcemaxiter={}'.format(
                self.input.forcemaxiter_value)

        if self.input.use_optS1st:
            als_arguments += ', optS1st={}'.format(self.input.optS1st_value)

        if self.input.use_baseline:
            als_arguments += ', baseline={}'.format(self.input.baseline_value)

        if self.input.use_nonnegS:
            als_arguments += ', nonnegS={}'.format(self.input.nonnegS_value)

        if self.input.use_nonnegC:
            als_arguments += ', nonnegC={}'.format(self.input.nonnegC_value)

        if self.input.use_uniS:
            als_arguments += ', uniS={}'.format(self.input.uniS_value)

        if self.input.use_uniC:
            als_arguments += ', uniC={}'.format(self.input.uniC_value)

        if self.input.use_normS:
            als_arguments += ', normS={}'.format(self.input.normS_value)

        print('als function arguments:', als_arguments, '\n')

        print('*** Performing multivariate curve resolution analysis...')
        t0 = time.time()

        # Perform MCR analysis by calling R software.
        mcr_result = eval('ALSals({})'.format(als_arguments))

        # Convert objects from R to Python format.
        self.S = np.array(mcr_result.rx2('S'), dtype=np.float)
        self.C = np.array(mcr_result.rx2('CList')[0], dtype=np.float)
        self.RSS = np.float(mcr_result.rx2('rss')[0])

        print('    {:g} seconds for performing multivariate curve resolution '
              'analysis.'.format(time.time() - t0))
        print('*** Finished performing multivariate curve resolution '
              'analysis.\n')

    def match(self):
        """Perform match of decomposed spectra against database."""

        r = robjects.r
        ALS = importr('ALS')
        ALSmatchFactor = r['matchFactor']

        self.filter_database()
        n_candidates = min(NUMBER_CANDIDATES, len(self.database_filtered_id))

        try:
            assert n_candidates > 0
        except AssertionError:
            print('AssertionError: Number of candidates should be larger '
                  'than zero.')
            raise

        print('*** Performing search-match process...')
        t0 = time.time()

        self.best_matches = []
        ndim = self.input.number_of_components
        for base_id in range(ndim):
            QtCore.QCoreApplication.processEvents()
            self.best_matches.append(Candidates())
            for name_id, name in enumerate(self.database.files):
                QtCore.QCoreApplication.processEvents()
                if name_id in self.database_filtered_id:
                    numpy2ri.activate()
                    mf = ALSmatchFactor(
                        self.S[:, base_id],
                        self.database.intensity_matrix[name_id, :])
                    numpy2ri.deactivate()
                    match_factor = mf[0]
                    candidate = Candidate(name, name_id, match_factor)
                    len_list = len(self.best_matches[base_id].candidate_list)
                    if  len_list < n_candidates:
                        self.best_matches[base_id].candidate_list.append(
                            candidate)
                    elif match_factor > \
                            self.best_matches[base_id].candidate_list[-1].match_factor:
                        self.best_matches[base_id].candidate_list[-1] = candidate
                    self.best_matches[base_id].sort()

        print('    {:g} seconds for performing search-match process.'.format(
            time.time() - t0))
        print('*** Finished performing search-match process.\n')

    def plot_cand_args(self, component_id, candidate_id, normalized=True):
        """Return plot info concerning the candidate_id-esim candidate of the
        component_id-esim component spectrum.

        Args:
            component_id (int): Component index.
            candidate_id (int): Candidate index.
            normalized (bool): Define if plot must be normalized.

        Returns:
            dict: Dictionary with plot info.
        """

        try:
            assert isinstance(component_id, int)
        except AssertionError:
            print('AssertionError: Argument component_id ({}) sould be of '
                  'int type.'.format(component_id))
            raise

        try:
            assert isinstance(candidate_id, int)
        except AssertionError:
            print('AssertionError: Argument candidate_id ({}) sould be of '
                  'int type.'.format(candidate_id))
            raise

        try:
            assert isinstance(normalized, bool)
        except AssertionError:
            print('AssertionError: Argument normalized ({}) sould be of bool '
                  'type.'.format(normalized))
            raise

        Rs = self.database.Rs
        candidate = \
            self.best_matches[component_id].candidate_list[candidate_id]
        database_id = candidate.database_id
        name = candidate.name.split('/')[-1]
        match_factor = candidate.match_factor
        I = self.database.intensity_matrix[database_id, :]
        if normalized:
            I = I/max(I)
        arguments = {'x': Rs,
                     'y': I,
                     'color': 'red',
                     'label': '{} (match factor = {:5.3f})'.format(
                         name, match_factor)}

        return arguments

    def plot_comp_args(self, component_id, normalized=True):
        """Return plot info concerning the component_id-esim component
        spectrum.

        Args:
            component_id (int): Component index.
            normalized (bool): Define if plot must be normalized.

        Returns:
            dict: Dictionary with plot info.
        """

        try:
            assert isinstance(component_id, int)
        except AssertionError:
            print('AssertionError: Argument component_id ({}) sould be of '
                  'int type.'.format(component_id))
            raise

        try:
            assert isinstance(normalized, bool)
        except AssertionError:
            print('AssertionError: Argument normalized ({}) sould be of bool '
                  'type.'.format(normalized))
            raise

        Rs = self.database.Rs
        I = self.S[:, component_id]
        if normalized:
            I = I/max(I)
        arguments = {'x': Rs,
                     'y': I,
                     'color': 'steelblue',
                     'label': 'Component spectrum'}

        return arguments

    def plot_calc_args(self, experiment_id):
        """Return plot info concerning the calculated spectrum associated to
        the experiment_id-esim experimental spectrum.

        Args:
            experiment_id (int): Index of the experimental spectrum whose
                associated calculated spectrum is to be plotted.

        Returns:
            dict: Dictionary with plot info.
        """

        try:
            assert isinstance(experiment_id, int)
        except AssertionError:
            print('AssertionError: Argument experiment_id ({}) sould be of '
                  'int type.'.format(experiment_id))
            raise

        Rs = self.experiment.Rs
        int_array = np.arange(0, len(self.experiment.files), 1)
        mcr_id = np.where(np.delete(int_array, self.intensity_indices)
                          == experiment_id)[0][0]
        I = (np.dot(self.C, np.transpose(self.S))[mcr_id, :])
        if self.input.baseline_fitting:
            I = I + baseline_als(
                self.experiment.intensity_matrix[experiment_id, :],
                self.input.baseline_fitting_lambda,
                self.input.baseline_fitting_p,
                self.input.baseline_fitting_tol,
                self.input.baseline_fitting_max_iter)
            label = 'Calculated from MCR with baseline summed (p = {:8.2e} ' \
                    'and lambda = {:8.2e}) '.format(
                        self.input.baseline_fitting_p,
                        self.input.baseline_fitting_lambda)
        else:
            label = 'Calculated from MCR'
        arguments = {'x': Rs,
                     'y': I,
                     'color': 'darkblue',
                     'label': label}

        return arguments

    def plot_calculated(self, experiment_id):
        """Plot the calculated spectrum associated to the experiment_id-esim
        experimental spectrum.

        Args:
            experiment_id (int): Index of the experimental spectrum whose
                associated calculated spectrum is to be plotted.
        """

        import matplotlib.pyplot as plt

        try:
            assert isinstance(experiment_id, int)
        except AssertionError:
            print('AssertionError: Argument experiment_id ({}) sould be of '
                  'int type.'.format(experiment_id))
            raise

        plt.figure()
        plt.xlabel('Raman shift')
        plt.ylabel('Intensity')
        plot_arguments = self.plot_calc_args(experiment_id)
        plot_from_args(plot_arguments)

    def plot_comp_contrib_args(self, experiment_id, component_id):
        """Return plot info concerning the contribution of the
        component_id-esim component spectrum to the calculated spectrum
        associated to the experiment_id-esim experimental spectrum.

        Args:
            experiment_id (int): Index of the experimental spectrum.

            component_id (int): Index of the component spectrum whose
                contribution to the calculated spectrum associated to the
                experiment_id-esim experimental spectrum is to be plotted.

        Returns:
            dict: Dictionary with plot info.
        """

        try:
            assert isinstance(experiment_id, int)
        except AssertionError:
            print('AssertionError: Argument experiment_id ({}) sould be of '
                  'int type.'.format(experiment_id))
            raise

        try:
            assert isinstance(component_id, int)
        except AssertionError:
            print('AssertionError: Argument component_id ({}) sould be of '
                  'int type.'.format(component_id))
            raise

        Rs = self.experiment.Rs
        int_array = np.arange(0, len(self.experiment.files), 1)
        mcr_id = np.where(np.delete(int_array, self.intensity_indices)
                          == experiment_id)[0][0]
        I = self.C[mcr_id, component_id] * \
            np.transpose(self.S)[component_id, :]
        if self.input.baseline_fitting:
            I = I + baseline_als(
                self.experiment.intensity_matrix[experiment_id, :],
                self.input.baseline_fitting_lambda,
                self.input.baseline_fitting_p,
                self.input.baseline_fitting_tol,
                self.input.baseline_fitting_max_iter)
        matched = self.best_matches[component_id].matched
        if matched is not None:
            match = self.best_matches[component_id].candidate_list[matched]
            name = match.name.split('/')[-1]
        else:
            name = 'not matched yet'
        arguments = {'x': Rs,
                     'y': I,
                     'color': 'red',
                     'label': 'Component spectrum {:d} ({})'.format(
                         component_id, name)}

        return arguments

    def get_report_string(self):
        """Create and return string for report about matched phases.

        Returns:
            str: Report about matched phases.
        """

        max_length = 7 # Length of string 'MATCHED'.
        for i in range(self.input.number_of_components):
            if self.best_matches[i].matched is not None:
                length = len(self.best_matches[i].candidate_list[
                    self.best_matches[i].matched].name.split('/')[-1])
                max_length = max(length, max_length)
        string = '='*(max_length + 21) + '\n'
        string_format = '{0:^9} | {1:^{2}} | {3:^6}\n'
        string += string_format.format(
            'COMPONENT', 'PHASE', max_length, 'MATCH')
        string += string_format.format(
            'SPECTRUM', 'MATCHED', max_length, 'FACTOR')
        string += '='*(max_length + 21) + '\n'
        for i in range(self.input.number_of_components):
            matched = self.best_matches[i].matched
            if matched is not None:
                name = self.best_matches[i].candidate_list[
                    matched].name.split('/')[-1]
                match_factor = self.best_matches[i].candidate_list[
                    matched].match_factor
                string += '{0:^9d} | {1:^{2}} | {3:^6.3f}\n'.format(
                    i, name, max_length, match_factor)
            else:
                string += '{0:^9d} | {1:^{2}} | {3:^6}\n'.format(
                    i, 'None', max_length, 'None')
        string += '='*(max_length + 21)

        return string

    def report(self):
        """Create report about matched phases."""

        print(self.get_report_string())


if __name__ == '__main__':
    import doctest
    doctest.testmod()
