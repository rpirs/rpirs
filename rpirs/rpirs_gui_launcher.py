"""
The launcher for a GUI for robust phase identification by Raman spectroscopy
(November 29th, 2017).

.. codeauthor:: Gustavo R. Ramos <grramos@ucs.br>

Warning:
    In xxx_save methods, the file extension is not being appended to the file
    name automatically (at least in Linux), i.e., the file extension is
    appended only if the user sets it in the dialog. The extension could be
    appended manually, but in this case the dialog may not warn the user when
    the file will be overwritten. Then, for safety reasons, and since this
    seems to be a bug of getSaveFileName method, the code will be kept as it
    is.

Todo:
    IPython console hangs when GUI is closed. When the changes made to output
    stream are commented, the GUI closes successfully. Fix this problem.

Todo:
    Check if time.time() is the better option for measuring a process time.
"""

import sys
import time
import numpy as np
import rpirs
from rpirs_gui import Ui_MainWindow # Class created with Qt 4 Designer and
                                    # converted with pyuic5
import matplotlib
matplotlib.use('Qt5Agg')
from PyQt5 import QtWidgets, QtCore, QtGui
from PyQt5.QtWidgets import QFileDialog, QVBoxLayout, QMainWindow
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg, \
        NavigationToolbar2QT

STRING_EXP_DB_INPUT_FILES = ('# directory\n'
                             '{}\n'
                             '# file_names\n'
                             '{}\n'
                             '# column_delimiter\n'
                             '{}\n'
                             '# Rs_min\n'
                             '{}\n'
                             '# Rs_step\n'
                             '{}\n'
                             '# Rs_max\n'
                             '{}')

STDOUT = sys.stdout
STDERR = sys.stderr

class EmittingStream(QtCore.QObject):
    """Stream that emits a signal when data is written to it.

    Note:
        Later an instance of this class will be assigned to sys.stdout and
        another instance will be assigned to sys.stderr. The default value of
        sys.stdout (sys.stderr) is sys.__stdout__ (sys.__stderr__), which has
        a method named write. The definition below is therefore a kind of
        redefinition of such a method. So, a signal will be emitted when
        trying to write to standard output (e.g.: print statement).
    """

    textWritten = QtCore.pyqtSignal(str)

    def write(self, text):
        self.textWritten.emit(str(text))


class PlotCanvas(FigureCanvasQTAgg):
    """Simple canvas the figure renders into.

    Args:
        parent (QWidget): Parent widget.
        width (float): Width of figure, in inches.
        height (float): Height of figure, in inches.
        dpi (float): Figure resolution, in dots per inch.
    """

    def __init__(self, parent=None, width=7, height=4, dpi=100):

        # Create figure.
        fig = matplotlib.figure.Figure(
            figsize=(width, height), dpi=dpi, facecolor='#fcf9f6',
            tight_layout=True)
        self.axes = fig.add_subplot(111)
        self.axes.set_xlabel('Raman shift')
        self.axes.set_ylabel('Intensity')
        FigureCanvasQTAgg.__init__(self, fig)
        self.setParent(parent)


class RpirsGui(Ui_MainWindow):
    """rpirs GUI class.

    Args:
        win (PyQt5.QtWidgets.QMainWindow instance): QMainWindow.
        (Others): See parent class.
    """

    def __init__(self, win):

        self.win = win
        self.setupUi(win)

#==============================================================================
# Experiment tab
#==============================================================================
        exp = \
            ((self.Rs_min, 'textChanged', self.exp_update),
             (self.Rs_step, 'textChanged', self.exp_update),
             (self.Rs_max, 'textChanged', self.exp_update),
             (self.exp_directory, 'textChanged', self.exp_update),
             (self.exp_browse, 'clicked', self.exp_browse_mtd),
             (self.exp_file_names, 'textChanged', self.exp_update),
             (self.exp_column_delimiter, 'textChanged', self.exp_update),
             (self.exp_load, 'clicked', self.exp_load_mtd),
             (self.exp_save, 'clicked', self.exp_save_mtd),
             (self.exp_restore, 'clicked', self.exp_restore_mtd),
             (self.exp_delete, 'clicked', self.exp_delete_mtd))
        for element in exp:
            getattr(element[0], element[1]).connect(element[2])

#==============================================================================
# Database tab
#==============================================================================
        db = ((self.Rs_min, 'textChanged', self.db_update),
              (self.Rs_step, 'textChanged', self.db_update),
              (self.Rs_max, 'textChanged', self.db_update),
              (self.db_directory, 'textChanged', self.db_update),
              (self.db_browse, 'clicked', self.db_browse_mtd),
              (self.db_file_names, 'textChanged', self.db_update),
              (self.db_column_delimiter, 'textChanged', self.db_update),
              (self.db_load, 'clicked', self.db_load_mtd),
              (self.db_save, 'clicked', self.db_save_mtd),
              (self.db_restore, 'clicked', self.db_restore_mtd),
              (self.db_delete, 'clicked', self.db_delete_mtd))
        for element in db:
            getattr(element[0], element[1]).connect(element[2])

#==============================================================================
# MCR tab
#==============================================================================
        mcr = ((self.mcr_intensity_filter, 'stateChanged',
                self.mcr_update_global),
               (self.mcr_intensity, 'textChanged', self.mcr_update),
               (self.mcr_n, 'textChanged', self.mcr_update),
               (self.mcr_thresh_use, 'stateChanged', self.mcr_update_global),
               (self.mcr_thresh, 'textChanged', self.mcr_update),
               (self.mcr_maxiter_use, 'stateChanged', self.mcr_update_global),
               (self.mcr_maxiter, 'textChanged', self.mcr_update),
               (self.mcr_forcemaxiter_use, 'stateChanged',
                self.mcr_update_global),
               (self.mcr_forcemaxiter_True, 'toggled', self.mcr_update),
               (self.mcr_optS1st_use, 'stateChanged', self.mcr_update_global),
               (self.mcr_optS1st_True, 'toggled', self.mcr_update),
               (self.mcr_baseline_use, 'stateChanged', self.mcr_update_global),
               (self.mcr_baseline_True, 'toggled', self.mcr_update),
               (self.mcr_nonnegS_use, 'stateChanged', self.mcr_update_global),
               (self.mcr_nonnegS_True, 'toggled', self.mcr_update),
               (self.mcr_nonnegC_use, 'stateChanged', self.mcr_update_global),
               (self.mcr_nonnegC_True, 'toggled', self.mcr_update),
               (self.mcr_uniS_use, 'stateChanged', self.mcr_update_global),
               (self.mcr_uniS_True, 'toggled', self.mcr_update),
               (self.mcr_uniC_use, 'stateChanged', self.mcr_update_global),
               (self.mcr_uniC_True, 'toggled', self.mcr_update),
               (self.mcr_normS_use, 'stateChanged', self.mcr_update_global),
               (self.mcr_normS, 'textChanged', self.mcr_update),
               (self.mcr_chemical_filter, 'stateChanged',
                self.mcr_update_global),
               (self.mcr_chemical_elements, 'textChanged', self.mcr_update),
               (self.mcr_laser_filter, 'stateChanged', self.mcr_update_global),
               (self.mcr_laser_wavelength, 'currentTextChanged',
                self.mcr_update),
               (self.mcr_run, 'clicked', self.mcr_run_mtd),
               (self.mcr_save, 'clicked', self.mcr_save_mtd),
               (self.mcr_restore, 'clicked', self.mcr_restore_mtd),
               (self.mcr_delete, 'clicked', self.mcr_delete_mtd))
        for element in mcr:
            getattr(element[0], element[1]).connect(element[2])

#==============================================================================
# Plot tab
#==============================================================================
        plot = ((self.plot_subtab, 'currentChanged', self.plot_subtab_mtd),
                (self.plot_bl_fit, 'stateChanged', self.plot_bl_fit_mtd),
                (self.plot_bl_fit_p, 'textChanged', self.plot_bl_fit_mtd),
                (self.plot_bl_fit_lambda, 'textChanged',
                 self.plot_bl_fit_mtd),
                (self.plot_bl_fit_tol, 'textChanged', self.plot_bl_fit_mtd),
                (self.plot_bl_fit_max_iter, 'textChanged',
                 self.plot_bl_fit_mtd),
                (self.plot_intensity_filter, 'stateChanged',
                 self.plot_intensity_filter_mtd),
                (self.plot_intensity, 'textChanged', self.plot_intensity_mtd),
                (self.plot_component, 'textChanged', self.plot_component_mtd),
                (self.plot_component_sb, 'valueChanged',
                 self.plot_component_sb_mtd),
                (self.plot_db_search, 'clicked', self.plot_db_search_mtd),
                (self.plot_db_results, 'currentTextChanged',
                 self.plot_db_results_mtd),
                (self.plot_candidate, 'textChanged', self.plot_candidate_mtd),
                (self.plot_candidate_sb, 'valueChanged',
                 self.plot_candidate_sb_mtd),
                (self.plot_candidate_match, 'stateChanged',
                 self.plot_candidate_match_mtd),
                (self.plot_spectrum, 'textChanged', self.plot_spectrum_mtd),
                (self.plot_spectrum_sb, 'valueChanged',
                 self.plot_spectrum_sb_mtd))
        for element in plot:
            getattr(element[0], element[1]).connect(element[2])
        self.plot_exp_tab_enable(False)
        self.plot_db_tab_enable(False)
        self.plot_mcr_tab_enable(False)

#==============================================================================
# Configuration of graphics and toolbar
#==============================================================================
        # Graphics.
        layout = QVBoxLayout(self.widget_graphics)
        self.sc = PlotCanvas(self.widget_graphics, width=13.5, height=5.5,
                             dpi=100)
        layout.addWidget(self.sc)
        # Toolbar.
        layout_toolbar = QVBoxLayout(self.widget_graphics_toolbar)
        self.toolbar = NavigationToolbar2QT(self.sc,
                                            self.widget_graphics_toolbar)
        layout_toolbar.addWidget(self.toolbar)
        self.toolbar.setOrientation(QtCore.Qt.Vertical)

#==============================================================================
# Stream output to GUI (rather than to sys.__stdout__ and sys.__stderr__).
#==============================================================================

        # Install instances of EmittingStream to sys.stdout and sys.stderr and
        # connect the textWritten signal to self.Output.
        sys.stdout = EmittingStream(textWritten=self.Output)
        sys.stderr = EmittingStream(textWritten=self.Output)

    def Output(self, text):
        """Append text to log."""

        cursor = self.log.textCursor()
        cursor.movePosition(QtGui.QTextCursor.End)
        cursor.insertText(text)
        self.log.setTextCursor(cursor)
        self.log.ensureCursorVisible()

    def __del__(self):
        """Restore sys.stdout and sys.stderr."""

        # Restore sys.stdout and sys.stderr.
        sys.stdout = sys.__stdout__
        sys.stderr = sys.__stderr__

#==============================================================================
# Experiment, Database, and MCR methods
#==============================================================================

    def exp_browse_mtd(self):
        """"Browse experiment folder."""

        self.exp_directory.setText(
            str(QFileDialog.getExistingDirectory(self.win,
                "Select the directory where experimental spectra are "
                "stored.")))

    def db_browse_mtd(self):
        """"Browse database folder."""

        self.db_directory.setText(
            str(QFileDialog.getExistingDirectory(self.win,
                "Select the directory where database spectra are stored.")))

    def exp_update(self):
        """Update experiment input file shown in GUI and save file."""

        string = STRING_EXP_DB_INPUT_FILES.format(
            self.exp_directory.text(),
            self.exp_file_names.text(),
            self.exp_column_delimiter.text(),
            self.Rs_min.text(),
            self.Rs_step.text(),
            self.Rs_max.text())
        self.exp_input_file.setText(string)
        if self.exp_directory.text():
            with open('{}/Raman_temp_experiment.rpirs'.format(
                self.exp_directory.text()), mode='w', encoding='utf8') as f:
                print(string, file=f)

    def db_update(self):
        """Update database input file shown in GUI and save file."""

        string = STRING_EXP_DB_INPUT_FILES.format(
            self.db_directory.text(),
            self.db_file_names.text(),
            self.db_column_delimiter.text(),
            self.Rs_min.text(),
            self.Rs_step.text(),
            self.Rs_max.text())
        self.db_input_file.setText(string)
        if self.db_directory.text():
            with open('{}/Raman_temp_database.rpirs'.format(
                self.db_directory.text()), mode='w', encoding='utf8') as f:
                print(string, file=f)

    def mcr_update_global(self):
        """Update MCR options, MCR input file shown in GUI and save file."""

        self.mcr_update_options()
        self.mcr_update()

    def mcr_update_options(self):
        """Update MCR options"""

        widgets = ((self.mcr_intensity_filter, self.mcr_intensity),
                   (self.mcr_thresh_use, self.mcr_thresh),
                   (self.mcr_maxiter_use, self.mcr_maxiter),
                   (self.mcr_forcemaxiter_use, self.frame_mcr_forcemaxiter),
                   (self.mcr_optS1st_use, self.frame_mcr_optS1st),
                   (self.mcr_baseline_use, self.frame_mcr_baseline),
                   (self.mcr_nonnegS_use, self.frame_mcr_nonnegS),
                   (self.mcr_nonnegC_use, self.frame_mcr_nonnegC),
                   (self.mcr_uniS_use, self.frame_mcr_uniS),
                   (self.mcr_uniC_use, self.frame_mcr_uniC),
                   (self.mcr_normS_use, self.mcr_normS),
                   (self.mcr_chemical_filter, self.mcr_chemical_elements),
                   (self.mcr_laser_filter, self.mcr_laser_wavelength))
        for widget in widgets:
            widget[1].setEnabled(widget[0].isChecked())

    def mcr_update(self):
        """Update MCR input file shown in GUI and save file."""

        string_format = ('# baseline_fitting\n'
                         '{}\n'
                         '# baseline_fitting_p\n'
                         '{}\n'
                         '# baseline_fitting_lambda\n'
                         '{}\n'
                         '# baseline_fitting_tol\n'
                         '{}\n'
                         '# baseline_fitting_max_iter\n'
                         '{}\n'
                         '# intensity_filter\n'
                         '{}\n'
                         '# intensity_value\n'
                         '{}\n'
                         '# number_of_components\n'
                         '{}\n'
                         '# use_thresh\n'
                         '{}\n'
                         '# thresh_value\n'
                         '{}\n'
                         '# use_maxiter\n'
                         '{}\n'
                         '# maxiter_value\n'
                         '{}\n'
                         '# use_forcemaxiter\n'
                         '{}\n'
                         '# forcemaxiter_value\n'
                         '{}\n'
                         '# use_optS1st\n'
                         '{}\n'
                         '# optS1st_value\n'
                         '{}\n'
                         '# use_baseline\n'
                         '{}\n'
                         '# baseline_value\n'
                         '{}\n'
                         '# use_nonnegS\n'
                         '{}\n'
                         '# nonnegS_value\n'
                         '{}\n'
                         '# use_nonnegC\n'
                         '{}\n'
                         '# nonnegC_value\n'
                         '{}\n'
                         '# use_uniS\n'
                         '{}\n'
                         '# uniS_value\n'
                         '{}\n'
                         '# use_uniC\n'
                         '{}\n'
                         '# uniC_value\n'
                         '{}\n'
                         '# use_normS\n'
                         '{}\n'
                         '# normS_value\n'
                         '{}\n'
                         '# chemical_filter\n'
                         '{}\n'
                         '# chemical_elements\n'
                         '{}\n'
                         '# laser_filter\n'
                         '{}\n'
                         '# laser_wavelength\n'
                         '{}')
        string = string_format.format(
            self.plot_bl_fit.isChecked(),
            self.plot_bl_fit_p.text(),
            self.plot_bl_fit_lambda.text(),
            self.plot_bl_fit_tol.text(),
            self.plot_bl_fit_max_iter.text(),
            self.mcr_intensity_filter.isChecked(),
            self.mcr_intensity.text(),
            self.mcr_n.text(),
            self.mcr_thresh_use.isChecked(),
            self.mcr_thresh.text(),
            self.mcr_maxiter_use.isChecked(),
            self.mcr_maxiter.text(),
            self.mcr_forcemaxiter_use.isChecked(),
            self.mcr_forcemaxiter_True.isChecked(),
            self.mcr_optS1st_use.isChecked(),
            self.mcr_optS1st_True.isChecked(),
            self.mcr_baseline_use.isChecked(),
            self.mcr_baseline_True.isChecked(),
            self.mcr_nonnegS_use.isChecked(),
            self.mcr_nonnegS_True.isChecked(),
            self.mcr_nonnegC_use.isChecked(),
            self.mcr_nonnegC_True.isChecked(),
            self.mcr_uniS_use.isChecked(),
            self.mcr_uniS_True.isChecked(),
            self.mcr_uniC_use.isChecked(),
            self.mcr_uniC_True.isChecked(),
            self.mcr_normS_use.isChecked(),
            self.mcr_normS.text(),
            self.mcr_chemical_filter.isChecked(),
            self.mcr_chemical_elements.text(),
            self.mcr_laser_filter.isChecked(),
            self.mcr_laser_wavelength.currentText()[:-3])
        self.mcr_input_file.setText(string)
        if self.exp_directory.text():
            with open('{}/Raman_temp_mcr.rpirs'.format(self.exp_directory.text()),
                      mode='w', encoding='utf8') as f:
                print(string, file=f)

    def restore_widgets(self, tab):
        """Restore widgets.

        Args:
            tab (str): Define which widgets must be restored.
        """

        if tab == 'exp':
            elements = ((self.exp_directory, self.exp.input.directory),
                        (self.exp_file_names, self.exp.input.file_names),
                        (self.exp_column_delimiter,
                         self.exp.input.column_delimiter),
                        (self.Rs_min, str(self.exp.input.Rs_min)),
                        (self.Rs_step, str(self.exp.input.Rs_step)),
                        (self.Rs_max, str(self.exp.input.Rs_max)))
            for element in elements:
                element[0].setText(element[1])
        elif tab == 'db':
            elements = ((self.db_directory, self.db.input.directory),
                        (self.db_file_names, self.db.input.file_names),
                        (self.db_column_delimiter,
                         self.db.input.column_delimiter),
                        (self.Rs_min, str(self.db.input.Rs_min)),
                        (self.Rs_step, str(self.db.input.Rs_step)),
                        (self.Rs_max, str(self.db.input.Rs_max)))
            for element in elements:
                element[0].setText(element[1])
        elif tab == 'mcr':
            elements = \
                ((self.plot_bl_fit, 'setChecked',
                  self.mcr.input.baseline_fitting),
                 (self.plot_bl_fit_p, 'setText',
                  str(self.mcr.input.baseline_fitting_p)),
                 (self.plot_bl_fit_lambda, 'setText',
                  str(self.mcr.input.baseline_fitting_lambda)),
                 (self.plot_bl_fit_tol, 'setText',
                  str(self.mcr.input.baseline_fitting_tol)),
                 (self.plot_bl_fit_max_iter, 'setText',
                  str(self.mcr.input.baseline_fitting_max_iter)),
                 (self.mcr_intensity_filter, 'setChecked',
                  self.mcr.input.intensity_filter),
                 (self.mcr_intensity, 'setText',
                  str(self.mcr.input.intensity_value)),
                 (self.mcr_n, 'setText',
                  str(self.mcr.input.number_of_components)),
                 (self.mcr_thresh_use, 'setChecked',
                  self.mcr.input.use_thresh),
                 (self.mcr_thresh, 'setText',
                  str(self.mcr.input.thresh_value)),
                 (self.mcr_maxiter_use, 'setChecked',
                  self.mcr.input.use_maxiter),
                 (self.mcr_maxiter, 'setText',
                  str(self.mcr.input.maxiter_value)),
                 (self.mcr_forcemaxiter_use, 'setChecked',
                  self.mcr.input.use_forcemaxiter),
                 (self.mcr_forcemaxiter_True, 'setChecked',
                  self.mcr.input.forcemaxiter_value),
                 (self.mcr_optS1st_use, 'setChecked',
                  self.mcr.input.use_optS1st),
                 (self.mcr_optS1st_True, 'setChecked',
                  self.mcr.input.optS1st_value),
                 (self.mcr_baseline_use, 'setChecked',
                  self.mcr.input.use_baseline),
                 (self.mcr_baseline_True, 'setChecked',
                  self.mcr.input.baseline_value),
                 (self.mcr_nonnegS_use, 'setChecked',
                  self.mcr.input.use_nonnegS),
                 (self.mcr_nonnegS_True, 'setChecked',
                  self.mcr.input.nonnegS_value),
                 (self.mcr_nonnegC_use, 'setChecked',
                  self.mcr.input.use_nonnegC),
                 (self.mcr_nonnegC_True, 'setChecked',
                  self.mcr.input.nonnegC_value),
                 (self.mcr_uniS_use, 'setChecked',
                  self.mcr.input.use_uniS),
                 (self.mcr_uniS_True, 'setChecked',
                  self.mcr.input.uniS_value),
                 (self.mcr_uniC_use, 'setChecked',
                  self.mcr.input.use_uniC),
                 (self.mcr_uniC_True, 'setChecked',
                  self.mcr.input.uniC_value),
                 (self.mcr_normS_use, 'setChecked',
                  self.mcr.input.use_normS),
                 (self.mcr_normS, 'setText',
                  str(self.mcr.input.normS_value)),
                 (self.mcr_chemical_filter, 'setChecked',
                  self.mcr.input.chemical_filter),
                 (self.mcr_chemical_elements, 'setText',
                  ' '.join(self.mcr.input.chemical_elements)),
                 (self.mcr_laser_filter, 'setChecked',
                  self.mcr.input.laser_filter))
            for element in elements:
                getattr(element[0], element[1])(element[2])
            for i in range(self.mcr_laser_wavelength.count()):
                self.mcr_laser_wavelength.setCurrentIndex(i)
                if self.mcr_laser_wavelength.currentText() \
                        == str(self.mcr.input.laser_wavelength) + ' nm':
                    break
            else:
                self.mcr_laser_wavelength.addItem(str(self.mcr.input.laser_wavelength) + ' nm')
                self.mcr_laser_wavelength.setCurrentIndex(self.mcr_laser_wavelength.count())

#==============================================================================
# Tab updating methods
#==============================================================================

    def plot_subtab_update_after_lrr(self, tab):
        """Update plot tab after loading, running, or restoring methods.

        Args:
            tab (str): Must be equal to either 'plot_exp_tab', 'plot_db_tab'
                or 'plot_mcr_tab' and define which tab must be updated (and
                set).
        """

        self.tab_right.setCurrentIndex(0)
        if tab == 'plot_exp_tab':
            self.plot_subtab.setCurrentIndex(0)
            self.plot_exp_tab_enable(True)
        elif tab == 'plot_db_tab':
            self.plot_subtab.setCurrentIndex(1)
            self.plot_db_tab_enable(True)
        elif tab == 'plot_mcr_tab':
            self.plot_subtab.setCurrentIndex(2)
            self.plot_exp_tab_enable(True)
            self.plot_mcr_tab_enable(True)

    def plot_exp_tab_enable(self, enable):
        """Enable or disable tab Experiment.

        Args:
            enable (bool): Equal to True if tab must be enable or equal to
                False if tab must be disabled.
        """

        self.plot_exp_tab.setEnabled(enable)
        self.plot_exp_subtab.setEnabled(enable)
        # Tab Baseline Fitting:
        widgets = (self.plot_bl_fit_tab,
                   self.plot_bl_fit)
        for widget in widgets:
            widget.setEnabled(enable)
        widgets = (self.plot_bl_fit_p_label,
                   self.plot_bl_fit_p,
                   self.plot_bl_fit_lambda_label,
                   self.plot_bl_fit_lambda,
                   self.plot_bl_fit_tol_label,
                   self.plot_bl_fit_tol,
                   self.plot_bl_fit_max_iter_label,
                   self.plot_bl_fit_max_iter)
        for widget in widgets:
            widget.setEnabled(self.plot_bl_fit.isChecked())
        # Tab Intensity Filter:
        widgets = (self.plot_intensity_filter_tab,
                   self.plot_intensity_filter)
        for widget in widgets:
            widget.setEnabled(enable)
        self.plot_intensity.setEnabled(self.plot_intensity_filter.isChecked())
        if self.plot_intensity_filter.isChecked():
            self.find_plot_intensity_indices()
            if not self.plot_intensity_indices:
                print('WARNING: Intensity value is too high. Set a lower '
                      'value. Resetting to 0...')
                self.plot_intensity.setText('0')

        # Tab Component:
        widgets = (self.plot_component_tab,
                   self.plot_component_label,
                   self.plot_component_sb,
                   self.plot_component)
        for widget in widgets:
            widget.setEnabled(enable and hasattr(self, 'mcr'))

    def plot_db_tab_enable(self, enable):
        """Enable or disable tab Database.

        Args:
            enable (bool): Equal to True if tab must be enable or equal to
                False if tab must be disabled.
        """

        widgets = (self.plot_db_tab,
                   self.plot_db_label,
                   self.plot_db_entry,
                   self.plot_db_results,
                   self.plot_db_search)
        for widget in widgets:
            widget.setEnabled(enable)

    def plot_mcr_tab_enable(self, enable):
        """Enable or disable tab MCR.

        Args:
            enable (bool): Equal to True if tab must be enable or equal to
                False if tab must be disabled.
        """

        widgets = (self.plot_mcr_tab,
                   self.plot_candidate_label,
                   self.plot_candidate_sb,
                   self.plot_candidate,
                   self.plot_candidate_match)
        for widget in widgets:
            widget.setEnabled(enable)

#==============================================================================
# plot_spectrum_sb_update method and "sub"-methods
#==============================================================================

    def plot_spectrum_sb_update(self, tab):
        """Update plot_spectrum_sb.

        Args:
            tab (str): Must be equal to either 'plot_exp_tab', 'plot_db_tab'
                or 'plot_mcr_tab' and define to which tab plot_spectrum_sb
                will correspond.
        """

        if tab == 'plot_exp_tab':
            self.plot_spectrum_sb_update_exp()
        elif tab == 'plot_db_tab':
            self.plot_spectrum_sb_update_db()
        elif tab == 'plot_mcr_tab':
            self.plot_spectrum_sb_update_mcr()

    def plot_spectrum_sb_update_exp(self):
        if self.plot_intensity_filter.isChecked():
            self.plot_spectrum_sb_update_exp_unusual()
        else:
            self.plot_spectrum_sb_update_exp_usual()

    def plot_spectrum_sb_update_exp_unusual(self):
        """
        Note:
            See note on method plot_spectrum_sb_update_exp_usual for
            explanation regarding the internal if clause.
        """

        if not hasattr(self, 'plot_id_exp_if'):
            self.plot_id_exp_if = 0
        if self.plot_id_exp_if > self.plot_spectrum_sb.maximum():
            self.plot_spectrum_sb.setMaximum(
                len(self.plot_intensity_indices)-1)
            self.plot_spectrum_sb.setValue(self.plot_id_exp_if)
        else:
            self.plot_spectrum_sb.setValue(self.plot_id_exp_if)
            self.plot_spectrum_sb.setMaximum(
                len(self.plot_intensity_indices)-1)

    def plot_spectrum_sb_update_exp_usual(self):
        """
        Note:
            Regarding the use of the last if clause, it is necessary for the
            GUI to keep current value of plot_id_exp. For instance: be the
            database the plot tab currently selected, and let's assume that
            it has 20 entries. Then let's suppose we want to select back
            experiment plot tab, for which previously selected spectrum was
            that whose index is 100. If only the statements in the else part
            of the clause were present, the new value of the experiment plot
            id would be 19 (the last index of database plot spectra), not 100,
            as desired.
        """

        if not hasattr(self, 'plot_id_exp'):
            self.plot_id_exp = 0
        if self.plot_id_exp > self.plot_spectrum_sb.maximum():
            self.plot_spectrum_sb.setMaximum(len(self.exp.files)-1)
            self.plot_spectrum_sb.setValue(self.plot_id_exp)
        else:
            self.plot_spectrum_sb.setValue(self.plot_id_exp)
            self.plot_spectrum_sb.setMaximum(len(self.exp.files)-1)

    def plot_spectrum_sb_update_db(self):
        """
        Note:
            See note on method plot_spectrum_sb_update_exp_usual for
            explanation regarding the last if clause.
        """

        if not hasattr(self, 'plot_id_db'):
            self.plot_id_db = 0
        if self.plot_id_db > self.plot_spectrum_sb.maximum():
            self.plot_spectrum_sb.setMaximum(len(self.db.files)-1)
            self.plot_spectrum_sb.setValue(self.plot_id_db)
        else:
            self.plot_spectrum_sb.setValue(self.plot_id_db)
            self.plot_spectrum_sb.setMaximum(len(self.db.files)-1)

    def plot_spectrum_sb_update_mcr(self):
        """
        Note:
            See note on method plot_spectrum_sb_update_exp_usual for
            explanation regarding the last if clause.
        """

        if not hasattr(self, 'plot_id_mcr'):
            self.plot_id_mcr = 0
        n_comp = self.mcr.input.number_of_components
        if self.plot_id_mcr > self.plot_spectrum_sb.maximum():
            self.plot_spectrum_sb.setMaximum(n_comp - 1)
            self.plot_spectrum_sb.setValue(self.plot_id_mcr)
        else:
            self.plot_spectrum_sb.setValue(self.plot_id_mcr)
            self.plot_spectrum_sb.setMaximum(n_comp - 1)

#==============================================================================
# Loading, running, and restoring methods
#==============================================================================

    def exp_load_restore_mtds_aux(self, restore):
        """Auxiliar method for updating GUI after loading or restoring an
        experiment.

        Args:
            restore(bool): Must be equal to True when restoring a previously
                saved experiment.
        """

        if restore:
            self.restore_widgets('exp')
        self.plot_subtab_update_after_lrr('plot_exp_tab')
        self.plot_spectrum_sb_update('plot_exp_tab')
        self.plot()
        self.exp_save.setEnabled(True)
        self.exp_delete.setEnabled(True)

    def db_load_restore_mtds_aux(self, restore):
        """Auxiliar method for updating GUI after loading or restoring a
        database.

        Args:
            restore(bool): Must be equal to True when restoring a previously
                saved database.
        """

        if restore:
            self.restore_widgets('db')
        self.plot_subtab_update_after_lrr('plot_db_tab')
        self.plot_spectrum_sb_update('plot_db_tab')
        self.plot()
        self.db_save.setEnabled(True)
        self.db_delete.setEnabled(True)

    def mcr_run_restore_mtds_aux(self, restore):
        """Auxiliar method for updating GUI after running or restoring a MCR
        analysis.

        Args:
            restore(bool): Must be equal to True when restoring a previously
                saved MCR analysis.
        """

        if restore:
            self.restore_widgets('mcr')
        self.plot_subtab_update_after_lrr('plot_mcr_tab')
        self.plot_spectrum_sb_update('plot_mcr_tab')
        self.plot_component_sb.setMaximum(
            self.mcr.input.number_of_components - 1)
        self.plot_candidate_sb.setMaximum(min(rpirs.NUMBER_CANDIDATES,
            len(self.mcr.database_filtered_id)) - 1)
        self.plot_candidate_match_update()
        self.plot()
        self.mcr_save.setEnabled(True)
        self.mcr_delete.setEnabled(True)
        self.report()

    def exp_load_mtd(self):
        """Load experimental spectra and update GUI."""

        self.exp = rpirs.SpectraExperiment(self.exp_directory.text() +
                                           '/Raman_temp_experiment.rpirs')
        self.exp.load()
        self.exp_load_restore_mtds_aux(restore=False)

    def db_load_mtd(self):
        """Load database spectra and update GUI."""

        self.db = rpirs.SpectraDatabase(self.db_directory.text() +
                                        '/Raman_temp_database.rpirs')
        self.db.load()
        self.db_load_restore_mtds_aux(restore=False)

    def mcr_run_mtd(self):
        """Rund MCR, perform search-match, and update GUI."""

        self.mcr = rpirs.MCR(self.exp_directory.text() +
                             '/Raman_temp_mcr.rpirs', self.exp, self.db)
        self.mcr.run()
        self.mcr.match()
        self.mcr_run_restore_mtds_aux(restore=False)

    def exp_restore_mtd(self):
        """Restore experiment object."""

        import pickle

        [file_name, file_names] = QFileDialog.getOpenFileName(
            self.win, 'Select an experiment *.pkl file', filter='*.pkl')
        with open(file_name, 'rb') as input_file:
            self.exp = pickle.load(input_file)
        self.exp_load_restore_mtds_aux(restore=True)

    def db_restore_mtd(self):
        """Restore database object."""

        import pickle

        [file_name, file_names] = QFileDialog.getOpenFileName(
            self.win, 'Select a database *.pkl file', filter='*.pkl')
        with open(file_name, 'rb') as input_file:
            self.db = pickle.load(input_file)
        self.db_load_restore_mtds_aux(restore=True)

    def mcr_restore_mtd(self):
        """Restore MCR object."""

        import pickle

        [file_name, file_names] = QFileDialog.getOpenFileName(
            self.win, 'Select an MCR *.pkl file', filter='*.pkl')
        with open(file_name, 'rb') as input_file:
            self.mcr = pickle.load(input_file)
        self.exp = self.mcr.experiment
        self.db = self.mcr.database
        self.exp_load_restore_mtds_aux(restore=True)
        self.db_load_restore_mtds_aux(restore=True)
        self.mcr_run_restore_mtds_aux(restore=True)

#==============================================================================
# Saving and deleting methods
#==============================================================================

    def exp_save_mtd(self):
        """Save experiment object."""

        import pickle

        [file_name, file_names] = QFileDialog.getSaveFileName(
            self.win, 'Select a directory and a file name', filter='*.pkl')

        print('*** Saving experiment object...')
        t0 = time.time()
        with open(file_name, 'wb') as output_file:
            pickle.dump(self.exp, output_file, pickle.HIGHEST_PROTOCOL)
        print('    {:g} seconds for saving experiment object.'.format(
            time.time() - t0))
        print('*** Finished saving experiment object.\n')


    def db_save_mtd(self):
        """Save database object."""

        import pickle

        [file_name, file_names] = QFileDialog.getSaveFileName(
            self.win, 'Select a directory and a file name', filter='*.pkl')

        print('*** Saving database object...')
        t0 = time.time()
        with open(file_name, 'wb') as output_file:
            pickle.dump(self.db, output_file, pickle.HIGHEST_PROTOCOL)
        print('    {:g} seconds for saving database object.'.format(
            time.time() - t0))
        print('*** Finished saving database object.\n')

    def mcr_save_mtd(self):
        """Save MCR object."""

        import pickle

        [file_name, file_names] = QFileDialog.getSaveFileName(
            self.win, 'Select a directory and a file name', filter='*.pkl')

        print('*** Saving MCR object...')
        t0 = time.time()
        with open(file_name, 'wb') as output_file:
            pickle.dump(self.mcr, output_file, pickle.HIGHEST_PROTOCOL)
        print('    {:g} seconds for saving MCR object.'.format(
            time.time() - t0))
        print('*** Finished saving MCR object.\n')

    def exp_delete_mtd(self):
        """Delete experiment object."""

        del self.exp
        del self.plot_id_exp
        if hasattr(self, 'plot_id_exp_if'):
            del self.plot_id_exp_if
        self.exp_save.setEnabled(False)
        self.exp_delete.setEnabled(False)
        self.plot_exp_tab_enable(False)

    def db_delete_mtd(self):
        """Delete database object."""

        del self.db
        del self.plot_id_db
        self.db_save.setEnabled(False)
        self.db_delete.setEnabled(False)
        self.plot_db_tab_enable(False)

    def mcr_delete_mtd(self):
        """Delete MCR object."""

        del self.mcr
        del self.plot_id_mcr
        self.mcr_save.setEnabled(False)
        self.mcr_delete.setEnabled(False)
        self.plot_mcr_tab_enable(False)
        self.report_text.setText('')

#==============================================================================
# Plot methods
#==============================================================================

    def plot_subtab_mtd(self):
        """Update self.plot_spectrum_sb and refresh graph (if it is not
        refreshed automatically).
        """

        old_value = self.plot_spectrum_sb.value()
        self.plot_spectrum_sb_update(
            self.plot_subtab.currentWidget().objectName())
        # If self.plot_spectrum_sb.value() does not change, graph is not
        # refreshed automatically.
        if self.plot_spectrum_sb.value() == old_value:
            self.plot()

    def plot_spectrum_sb_mtd(self):
        """Update self.plot_spectrum when self.plot_spectrum_sb changes."""

        self.plot_spectrum.setText(str(self.plot_spectrum_sb.value()))

    def plot_spectrum_mtd(self):
        """Update self.plot_spectrum_sb and perform expected tasks when
        self.plot_spectrum changes.
        """

        self.plot_spectrum_sb.setValue(np.int(self.plot_spectrum.text()))
        self.plot_id_xxx_update()
        if hasattr(self, 'mcr'):
            self.plot_candidate_match_update()
        self.plot()

    def plot_id_xxx_update(self):
        """Update either self.plot_id_exp, self.plot_id_db, self.plot_id_mcr,
        or self.plot_id_exp_if, and also the value of self.plot_candidate_sb.
        """

        plot_id = self.plot_spectrum_sb.value()
        widget = self.plot_subtab.currentWidget().objectName()
        if widget == 'plot_exp_tab':
            if self.plot_intensity_filter.isChecked():
                self.plot_id_exp_if = plot_id
            else:
                self.plot_id_exp = plot_id
        elif widget == 'plot_db_tab':
            self.plot_id_db = plot_id
        elif widget == 'plot_mcr_tab':
            self.plot_id_mcr = plot_id
            if self.mcr.best_matches[plot_id].matched is None:
                self.plot_candidate_sb.setValue(0)
            else:
                self.plot_candidate_sb.setValue(
                    self.mcr.best_matches[plot_id].matched)

    def plot_bl_fit_mtd(self):
        """Update GUI and MCR input file and plot spectrum when
        self.plot_bl_fit is checked/unchecked."""

        widgets = (self.plot_bl_fit_p,
                   self.plot_bl_fit_p_label,
                   self.plot_bl_fit_lambda,
                   self.plot_bl_fit_lambda_label,
                   self.plot_bl_fit_tol,
                   self.plot_bl_fit_tol_label,
                   self.plot_bl_fit_max_iter,
                   self.plot_bl_fit_max_iter_label)
        for widget in widgets:
            widget.setEnabled(self.plot_bl_fit.isChecked())
        self.mcr_update()
        self.plot()

    def plot_intensity_filter_mtd(self):
        """Perform tasks when self.plot_intensity_filter is checked/
        unchecked.
        """

        self.plot_intensity.setEnabled(self.plot_intensity_filter.isChecked())
        if self.plot_intensity_filter.isChecked():
            self.plot_intensity_mtd()
        else:
            old_value = self.plot_spectrum_sb.value()
            self.plot_spectrum_sb_update('plot_exp_tab')
            # If self.plot_spectrum_sb.value() does not change, graph is not
            # refreshed automatically.
            if self.plot_spectrum_sb.value() == old_value:
                self.plot()

    def plot_intensity_mtd(self):
        """Perform tasks when self.plot_intensity changes (and also when
        self.plot_intensity_filter is checked).
        """

        old_value = self.plot_spectrum_sb.value()
        self.find_plot_intensity_indices()
        if not self.plot_intensity_indices:
            print('WARNING: Intensity value is too high. Set a lower value. '
                  'Resetting to 0...\n')
            self.plot_intensity.setText('0')
        else:
            self.plot_spectrum_sb_update('plot_exp_tab')
            # If self.plot_spectrum_sb.value() does not change, graph is not
            # refreshed automatically.
            if self.plot_spectrum_sb.value() == old_value:
                self.plot()

    def plot_component_sb_mtd(self):
        """Update self.plot_component when self.plot_component_sb changes."""

        self.plot_component.setText(str(self.plot_component_sb.value()))

    def plot_component_mtd(self):
        """Update self.plot_component_sb and refresh plot when
        self.plot_component changes."""

        self.plot_component_sb.setValue(np.int(self.plot_component.text()))
        self.plot()

    def plot_db_search_mtd(self):
        """Search database."""

        import re

        self.plot_db_results.clear()
        for name in self.db.files:
            if re.search(self.plot_db_entry.text(), name.split('/')[-1],
                         re.IGNORECASE):
                self.plot_db_results.addItem(name.split('/')[-1])

    def plot_db_results_mtd(self):
        """Update plot when search occurence selection changes.

        Todo:
            If current item in self.plot_db_results is clicked (i.e., the
            selected item does not change), the graph is not refreshed. It
            should be fixed.
        """

        for name_id, name in enumerate(self.db.files):
            if name.split('/')[-1] == self.plot_db_results.currentText():
                self.plot_spectrum_sb.setValue(name_id)

    def plot_candidate_sb_mtd(self):
        """Update self.plot_candidate when self.plot_candidate_sb changes."""

        self.plot_candidate.setText(str(self.plot_candidate_sb.value()))

    def plot_candidate_mtd(self):
        """Update self.plot_candidate_sb and self.plot_candidate_match, and
        refresh graph when self.plot_candidate changes."""

        self.plot_candidate_sb.setValue(np.int(self.plot_candidate.text()))
        self.plot_candidate_match_update()
        self.plot()

    def plot_candidate_match_mtd(self):
        """Update info about matched phases and update report when
        self.plot_candidate_match changes."""

        if self.plot_candidate_match.isChecked():
            self.mcr.best_matches[self.plot_id_mcr].matched = \
                    self.plot_candidate_sb.value()
        else:
            if self.mcr.best_matches[self.plot_id_mcr].matched == \
                    self.plot_candidate_sb.value():
                self.mcr.best_matches[self.plot_id_mcr].matched = None
        self.report()

    def plot_candidate_match_update(self):
        """Update plot_candidate_match."""

        self.plot_candidate_match.setChecked(
            self.mcr.best_matches[self.plot_id_mcr].matched ==
            self.plot_candidate_sb.value())

#==============================================================================
# plot method and "sub"-methods
#==============================================================================

    def plot(self):
        """Plot experiment, database, component, and calculated spectra, as
        well as baseline.
        """

        widgets = (self.plot_spectrum_label,
                   self.plot_spectrum,
                   self.plot_spectrum_sb,
                   self.plot_bl_fit_p_label,
                   self.plot_bl_fit_p,
                   self.plot_bl_fit_lambda_label,
                   self.plot_bl_fit_lambda,
                   self.plot_bl_fit_tol_label,
                   self.plot_bl_fit_tol,
                   self.plot_bl_fit_max_iter_label,
                   self.plot_bl_fit_max_iter,
                   self.plot_intensity,
                   self.plot_candidate,
                   self.plot_candidate_sb,
                   self.plot_component,
                   self.plot_component_sb)
        for widget in widgets:
            widget.setEnabled(False)
        print('*** Plotting graphics...')
        methods = {'plot_exp_tab': self.plot_exp,
                   'plot_db_tab': self.plot_db,
                   'plot_mcr_tab': self.plot_mcr}
        methods[self.plot_subtab.currentWidget().objectName()]()
        for widget in widgets:
            widget.setEnabled(True)
        self.plot_intensity.setEnabled(self.plot_intensity_filter.isChecked())
        widgets = (self.plot_bl_fit_p_label,
                   self.plot_bl_fit_p,
                   self.plot_bl_fit_lambda_label,
                   self.plot_bl_fit_lambda,
                   self.plot_bl_fit_tol_label,
                   self.plot_bl_fit_tol,
                   self.plot_bl_fit_max_iter_label,
                   self.plot_bl_fit_max_iter)
        for widget in widgets:
            widget.setEnabled(self.plot_bl_fit.isChecked())
        print('*** Finished plotting graphics...\n')

    def plot_exp(self):
        """Plot experiment, component, and calculated spectra, as
        well as baseline.
        """

        self.sc.axes.clear()
        if self.plot_intensity_filter.isChecked():
            plot_id = self.plot_intensity_indices[self.plot_id_exp_if]
        else:
            plot_id = self.plot_id_exp
        self.plot_exp_exp(plot_id)
        if self.plot_bl_fit.isChecked():
            self.plot_exp_bl(plot_id)
        if hasattr(self, 'mcr'):
            if plot_id not in self.mcr.intensity_indices:
                self.plot_exp_mcr(plot_id)
        self.plot_aux()

    def plot_exp_exp(self, plot_id):
        """Plot experimental spectrum.

        Args:
            plot_id (int): Experimental spectrum index.
        """

        plot_arguments = self.exp.plot_arguments(plot_id)
        self.plot_from_args(plot_arguments)

    def plot_exp_bl(self, plot_id):
        """Plot baseline currently fitted to experimental spectrum.

        Args:
            plot_id (int): Experimental spectrum index.
        """

        bl_fit_params = (self.plot_bl_fit_p,
                         self.plot_bl_fit_lambda,
                         self.plot_bl_fit_tol,
                         self.plot_bl_fit_max_iter)
        errors = 0
        for param in bl_fit_params:
            try:
                np.float(param.text())
            except ValueError:
                errors += 1
        if errors == 0:
            # Plot is refreshed only when all baseline parameters can be
            # converted to float.
            I = rpirs.baseline_als(self.exp.intensity_matrix[plot_id, :],
                                   np.float(self.plot_bl_fit_lambda.text()),
                                   np.float(self.plot_bl_fit_p.text()),
                                   np.float(self.plot_bl_fit_tol.text()),
                                   np.int(self.plot_bl_fit_max_iter.text()))
            self.sc.axes.plot(self.exp.Rs, I, color='limegreen',
                              label='Current fitted baseline')

    def plot_exp_mcr(self, plot_id):
        """Plot calculated and component spectra.

        Args:
            plot_id (int): Experimental spectrum index.
        """

        # Plot calculated spectrum.
        plot_arguments = self.mcr.plot_calc_args(plot_id)
        self.plot_from_args(plot_arguments)

        # Plot component contribution to calculated spectrum.
        component = self.plot_component_sb.value()
        plot_arguments = self.mcr.plot_comp_contrib_args(plot_id, component)
        self.plot_from_args(plot_arguments)

    def plot_db(self):
        """Plot database spectrum."""

        self.sc.axes.clear()
        plot_arguments = self.db.plot_arguments(self.plot_id_db)
        self.plot_from_args(plot_arguments)
        self.plot_aux()

    def plot_mcr(self):
        """Plot component and database spectra."""

        self.sc.axes.clear()
        component = self.plot_id_mcr
        candidate = self.plot_candidate_sb.value()

        # Plot component spectrum.
        plot_arguments = self.mcr.plot_comp_args(component)
        self.plot_from_args(plot_arguments)

        # Plot candidate spectrum.
        plot_arguments = self.mcr.plot_cand_args(component, candidate)
        self.plot_from_args(plot_arguments)

        self.plot_aux(ylabel='Normalized intensity')

    def plot_from_args(self, plot_arguments):
        """Plot graph based on plot_arguments.

        Arguments:
            plot_arguments (dict): Dictionary containing keys 'x', 'y',
                'color', and 'label', which are used for plotting the figure.
        """

        self.sc.axes.plot(plot_arguments['x'],
                          plot_arguments['y'],
                          color=plot_arguments['color'],
                          label=plot_arguments['label'])

    def plot_aux(self, xlabel='Raman shift', ylabel='Intensity'):
        """Auxiliar function to set x label, y label, and legends, and draw
        the graph."""

        self.sc.axes.set_xlabel(xlabel)
        self.sc.axes.set_ylabel(ylabel)
        self.sc.axes.legend(fontsize='x-small')
        self.sc.draw()

    def find_plot_intensity_indices(self):
        """Find indices of experimental spectra to plot.

        Find the indices corresponding to experimental spectra in which all
        intensities are greater than the value defined in GUI.
        """

        print('*** Finding indices of experimental spectra to plot...')
        self.plot_intensity_indices = []
        for spectrum_id in range(self.exp.n_spectra):
            if (np.min(self.exp.intensity_matrix[spectrum_id, :]) >
                    np.float(self.plot_intensity.text())):
                self.plot_intensity_indices.append(spectrum_id)
        print('*** Finished finding indices of experimental spectra to '
              'plot.\n')

#==============================================================================
# Report method
#==============================================================================

    def report(self):
        """Create report about matched phases."""

        self.report_text.setText(self.mcr.get_report_string())

#==============================================================================
# Redefinition of closeEvent method
#==============================================================================
class QMainWindowNew(QMainWindow):
    """Class for redefining closeEvent."""

    def closeEvent(self, event):

        from PyQt5.QtWidgets import QMessageBox

        reply = QMessageBox.question(self, 'Message', "Do you want to quit?",
                                     QMessageBox.Yes | QMessageBox.No,
                                     QMessageBox.No)

        if reply == QMessageBox.Yes:
            # Recover default values of sys.stdout and sys.stderr
            sys.stdout = STDOUT
            sys.stderr = STDERR
            event.accept()
            print('Quiting...')
        else:
            event.ignore()


if __name__ == '__main__':

    app = QtWidgets.QApplication(sys.argv)
    win = QMainWindowNew()
    prog = RpirsGui(win)
    win.show()
    sys.exit(app.exec_())
