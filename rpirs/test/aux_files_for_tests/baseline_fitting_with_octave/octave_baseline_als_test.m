y = [1; 0.5; 2.0; 3.0; 2.5];
z = octave_baseline_als(y, 1e7, 0.05, 1e-6, 10)

y = [1; 2; 3; 4; 5];
z = octave_baseline_als(y, 1e7, 0.05, 1e-6, 10)

y = [1; 1; 1; 1; 1];
z = octave_baseline_als(y, 1e7, 0.05, 1e-6, 10)