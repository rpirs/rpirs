function z = octave_baseline_als(y, lambda, p, tol, maxiter)
  % Estimate baseline with asymmetric least squares
  m = length(y);
  D = diff(speye(m), 2);
  w = ones(m, 1);
  for it = 1:maxiter
    w0 = w;
    W = spdiags(w, 0, m, m);
    C = chol(W + lambda * D' * D);
    z = C \ (C' \ (w .* y));
    w = p * (y > z) + (1 - p) * (y < z);
    if max(abs((w - w0)./w0)) <= tol
      disp('Baseline fitting converged with the following iterations:')
      disp(it)
      return
    endif
  endfor
  disp('Baseline fitting reached maximum number of iterations:')
  disp(maxiter)
end