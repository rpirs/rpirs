Welcome to RPIRS (Robust Phase Identification by Raman Spectroscopy) tool's documentation!
==========================================================================================

This is the documentation of RPIRS (Robust Phase Identification by Raman Spectroscopy) tool, which is described in the article "A tool for assisting phase identification by micro‐Raman spectroscopy" (https://onlinelibrary.wiley.com/doi/full/10.1002/jrs.5467).
Its aim is to identify the phases in a sample by first decomposing its experimental Raman spectra into the so-called components and then searching/matching these components against a database of Raman spectra.
The decomposition of experimental into component spectra is performed by the alternating least squares multivariate curve resolution (ALS-MCR) technique, by calling the ALS package written in R language by Katharine M. Mullen.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Disclaimer
**********

.. warning::
    The author is not responsible for erroneous results obtained with RPIRS. The author acknowledges all suggestions and notification of possible bugs found in the program.

Information about updates (including bug fixes)
***********************************************

.. warning::
    To receive information about updates (including bug fixes), you can either (a) log into your Bitbucket account, go to https://bitbucket.org/rpirs/rpirs/src/master/ and click on the "More options" icon next to the "Clone" button, click on "Manage notifications" and then on "Watch this repository", select the desired options, and then click on "Close"; or (b) send an e-mail to grramos@ucs.br, asking for information about updates.

License
*******

This tool is licensed under GPL.

General warnings
****************

.. warning::
    Please read all warning boxes on this page before using RPIRS.

.. warning::
    It is assumed that RPIRS GUI will be run inside Spyder, which is supplied by the Anaconda distribution.

Quick installation and launching instructions for Linux
*******************************************************

.. note::
    The code was tested on Ubuntu 16.04 LTS, with Python 3.6, RPIRS 0.0.1, R 3.4.2, and ALS 0.0.6 (with nnls 1.4 and Iso 0.0.17).

.. note::
    If there is any problem during installation, please see section 'Detailed instructions about installation of Spyder and ALS (R) and rpy2 (Python) packages on Ubuntu' below.

1) Download Anaconda distribution from https://www.anaconda.com/download/#linux and then install it.

2) Paste the three following addresses into an internet browser in order to download the R packages:

 * https://cran.r-project.org/src/contrib/ALS_0.0.6.tar.gz
 * https://cran.r-project.org/src/contrib/nnls_1.4.tar.gz
 * https://cran.r-project.org/src/contrib/Iso_0.0-17.tar.gz

3) Open a terminal and run:

>>> conda install -c r rpy2 
>>> cd <Downloads_folder>
>>> sudo R CMD INSTALL -l ~/anaconda3/lib/R/library ALS_0.0.6.tar.gz
>>> sudo R CMD INSTALL -l ~/anaconda3/lib/R/library nnls_1.4.tar.gz
>>> sudo R CMD INSTALL -l ~/anaconda3/lib/R/library Iso_0.0-17.tar.gz

4) To launch RPIRS GUI, first open a terminal and run:

>>> spyder

5) Inside Spyder, open the file ./rpirs/rpirs_gui_launcher.py (found in this repository) and then run it to open the graphical user interface.

.. warning::
    See section "Using RPIRS" found below.

Quick installation and launching instructions for Windows
*********************************************************

.. warning::
    The code was not tested on Windows, but only on Ubuntu 16.04 LTS.

1) Download Anaconda distribution from https://www.anaconda.com/download/#download and then install it using default options.

2) Open a command prompt as administrator and run:

>>> conda install -c r rpy2
>>> R

3) Inside R environment just accessed, run:

>>> install.packages("ALS", dependencies = TRUE)

4) After the installation is completed, run:

>>> quit()

5) To launch RPIRS GUI, first run:

>>> spyder

6) Inside Spyder, open the file ./rpirs/rpirs_gui_launcher.py (found in this repository) and then run it to open the graphical user interface.

.. warning::
    See section "Using RPIRS" found below.

Detailed instructions about installation of Spyder and ALS (R) and rpy2 (Python) packages on Ubuntu
***************************************************************************************************

.. note::
    These instructions were tested on Ubuntu 16.04 LTS.

.. note::
    For using the GUI it is necessary to have the following installed:

    1) R + ALS package;
    2) Python + rpy2 package.

.. note::
    The instructions on this section are concerned only with the installation of the packages and assume that the GUI will be run inside Spyder, which is supplied by the Anaconda distribution.

So, firstly install Anaconda distribution (see https://www.anaconda.com).
Then, in a terminal, run

>>> conda install -c r rpy2

to install rpy2 package.

.. note::
    If conda command is not recognized, add the line "export PATH=~/anaconda3/bin:$PATH" (without the quotes) to ~/.bashrc file.
    Please note that the folder *anaconda3* will possibly need to be changed.

This seems to install also the R program in conda environment.
Then, for installing the ALS package inside the R installation which is called from Spyder (even when there is another R installation on the system), first download the file ALS*.tar.gz found in https://cran.r-project.org/src/contrib.
(The tests were performed with ALS_0.0.6.tar.gz version).
Then, run, in a terminal:

>>> cd <path_to_folder>
>>> sudo R CMD INSTALL -l ~/anaconda3/lib/R/library <file_name>.tar.gz

where <path_to_folder> stands for the folder that the file mentioned above was downloaded into and <file_name> stands for the name of the tar.gz file.

.. note::
    The folder *anaconda3* will possibly need to be changed.

Since the dependencies of the package are not installed automatically, the same procedure, i.e.,

>>> cd <path_to_folder>
>>> sudo R CMD INSTALL -l ~/anaconda3/lib/R/library <file_name>.tar.gz

needs to be applied to packages nnls and Iso, needed for using the ALS package.

Code and documentation
**********************

The docstrings are in accordance with http://www.sphinx-doc.org/pt_BR/stable/ext/napoleon.html.

How to (re)generate documentation
*********************************

The files ./doc/conf.py and ./doc/rst/index.rst contain the information needed for generating the documentation with Sphinx.
If needed, these files can be changed (see http://www.sphinx-doc.org/en/stable/tutorial.html for instructions).

The documentation can be (re)generated by running, in a terminal, the following commands:

>>> cd <module_folder>/doc
>>> make html

Database
********

.. warning::
    In order to perform the search-match of the component spectra against the RRUFF database, it is necessary to download it.
    This can be done by accessing http://rruff.info/zipped_data_files/raman/, and then downloading the desired database(s).

Using RPIRS
***********

The graphical user interface (GUI) was built on Ubuntu with Qt Designer version 4.8.7.

.. todo::
    In https://rpy2.github.io/doc/v2.9.x/html/overview.html, it is reported that rpy2.robjects is a "high-level interface, when ease-of-use matters most".
    On the other hand, rpy2.rinterface is a "low-level interface to R, when speed and flexibility matters most."
    Current version of program is using rpy2.robjects.
    It seems that it would be interesting to use rpy2.rinterface.

.. warning::
    The possible column delimiters are unary symbols (such as ','), space(s) (which must be entered as ' '), and tab (which must be entered as '\\t').

.. warning::
    Be sure that all files read in this program are encoded in utf-8. Otherwise, reading problems may occur.

.. warning::
    The statement np.arange is used for generating Raman shift values.
    Its behaviour, however, is somewhat strange, since both np.arange(1.0, 2.4, 0.2, float) and np.arange(1.0, 2.2, 0.2, np.float) produce the following output: array([ 1. ,  1.2,  1.4,  1.6,  1.8,  2. ,  2.2]).
    This is, in fact, foreseen in the manual, as the description of the stop value reads "End of interval. The interval does not include this value, except in some cases where step is not an integer and floating point round-off affects the length of out.".
    For a better control, it is probably better to use numpy.linspace, since it is described as "Evenly spaced numbers with careful handling of endpoints.".

.. warning::
    The initial guesses for matrices S and C in multivariate curve resolution analysis are random matrices in RPIRS.
    Accordingly, the results of MCR analysis may differ each time the analysis is run.

.. warning::
    If the database is filtered by chemical composition, entries whose comment lines do not contain the expression *IDEAL CHEMISTRY* aren't filtered out.

.. warning::
    Please note that the usage of als function optional arguments (which appear on *INPUT INFORMATION -> MCR Analysis/Search Options -> ALS-MCR Analysis Options* as checkboxes) **were not** tested.

.. warning::
    Check that the *Log* tab of *LOG/INPUT FILES GENERATED* window is working properly.
    This log must display information about:

    - the processes being performed (such as when you load experimental or database spectra, run the MCR/search analysis, each time a baseline is fitted to an experimental spectrum, and so on); and
    - possibly, some warnings (such as the one regarding a file that isn't filtered out by chemical composition due to the fact that the expression *IDEAL CHEMISTRY* was not found in that entry).

    If the log either:

    - does not work (i.e., it does not display the kind of information just described); or
    - works one time you open RPIRS GUI but does not work another time you open RPIRS GUI,

    to get the log working, you may want to try the following:

    1. Close RPIRS GUI;
    2. In Spyder menu, go to *Tools -> Preferences -> IPython console -> Graphics -> Graphics backend -> Backend* and change the current option (keep it in mind in case you want to restore it after using RPIRS GUI) to *Qt5*, press *Apply* and then *Ok*;
    3. Close and re-open Spyder, and relaunch RPIRS GUI (the log should now work).
    4. After using RPIRS GUI, you may want to restore the "old" value of *Tools -> Preferences -> IPython console -> Graphics -> Graphics backend -> Backend*.

Indices and tables
******************

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

src
***

.. toctree::
   :maxdepth: 4

   rpirs
   rpirs_gui
   rpirs_gui_launcher

Todo List
*********

.. todolist::

